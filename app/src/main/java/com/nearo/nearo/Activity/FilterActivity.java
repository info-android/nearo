package com.nearo.nearo.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.nearo.nearo.R;

import static com.nearo.nearo.Helper.Constants.CURRENCY;
import static com.nearo.nearo.Helper.Constants.FILTER_MAX;
import static com.nearo.nearo.Helper.Constants.FILTER_MIN;
import static com.nearo.nearo.Helper.Constants.HIGHEST_PRICE;
import static com.nearo.nearo.Helper.Constants.IS_SORT;
import static com.nearo.nearo.Helper.Constants.LOWEST_PRICE;

public class FilterActivity extends AppCompatActivity {
    ImageView back;
    private CrystalRangeSeekbar rangeSeekbar5;
    private TextView tvMin,tvMax;
    String min_price;
    String max_price;
    private Button filter,cancel;
    private RadioGroup pricetype;
    String tmp_price_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        rangeSeekbar5 = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar5);
        // get min and max text view
        tvMin = (TextView) findViewById(R.id.textMin1);
        filter = (Button) findViewById(R.id.filter);
        cancel = (Button) findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterActivity.super.onBackPressed();
            }
        });
        tvMax = (TextView) findViewById(R.id.textMax1);
        max_price = getIntent().getStringExtra(HIGHEST_PRICE);
        min_price = getIntent().getStringExtra(LOWEST_PRICE);
        int min = 0;
        int max = 100000;
        try {
            min = Math.round(Float.parseFloat(min_price));
            max = Math.round(Float.parseFloat(max_price)) + 1;
        } catch (Exception e) {
            min = 0;
            max = 100000;
        }

        rangeSeekbar5.setMinValue(min);
        rangeSeekbar5.setMaxValue(max);
        tvMin.setText(CURRENCY + String.valueOf(min));
        tvMax.setText(CURRENCY + String.valueOf(max) + ' ');
        // set listener
        rangeSeekbar5.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(CURRENCY + String.valueOf(minValue));
                tvMax.setText(CURRENCY + String.valueOf(maxValue));
                max_price = String.valueOf(maxValue);
                min_price = String.valueOf(minValue);
            }
        });
        Log.e("FILTER_MIN2", FILTER_MIN );
        if(!FILTER_MIN.equals("") && !FILTER_MAX.equals("")){
            rangeSeekbar5.setMinStartValue(Float.parseFloat(FILTER_MIN));
            rangeSeekbar5.setMaxStartValue(Float.parseFloat(FILTER_MAX));
            rangeSeekbar5.apply();
        }

        // set final value listener
        rangeSeekbar5.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                Log.d("CRS=>", String.valueOf(minValue) + " : " + String.valueOf(maxValue));
            }
        });
        pricetype = (RadioGroup) findViewById(R.id.pricetype);
        pricetype.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                if (checkedId == R.id.high) {
                    tmp_price_type = "max";
                }
                if (checkedId == R.id.low) {
                    tmp_price_type = "min";
                }


            }
        });
        if(IS_SORT.equals("max")){
            pricetype.check(R.id.high);
        }else if(IS_SORT.equals("min")){
            pricetype.check(R.id.low);
        }
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FILTER_MAX = max_price;
                FILTER_MIN = min_price;
                IS_SORT = tmp_price_type;
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
    public void back(View view){
        super.onBackPressed();
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }

}
