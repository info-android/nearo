package com.nearo.nearo.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class EditProfileActivity extends AppCompatActivity {
    private Boolean is_logged_in = false;

    private TextView placeName;

    private EditText fname,lname,state,city,pin,phone,mobile,email,address,oldPass,newPass,confPass;
    private Spinner spinn;
    private String user_id = "";
    private Button save;
    private int selected = 0;
    private String coontry_ids = "";

    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        bottumLaySetColor("more");

        placeName = findViewById(R.id.placeName);
        fname = findViewById(R.id.fname);
        lname = findViewById(R.id.lname);
        state = findViewById(R.id.state);
        city = findViewById(R.id.city);
        pin = findViewById(R.id.pin);
        phone = findViewById(R.id.phone);
        mobile = findViewById(R.id.mobile);
        oldPass = findViewById(R.id.oldPass);
        address = findViewById(R.id.address);
        newPass = findViewById(R.id.newPass);
        confPass = findViewById(R.id.confPass);
        email = findViewById(R.id.email);
        spinn = findViewById(R.id.country_spinner);
        save = findViewById(R.id.save);


        MethodClass.placeName(EditProfileActivity.this,placeName);

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(EditProfileActivity.this).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(EditProfileActivity.this).getString("user_id","0");
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
            }
        });

        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(EditProfileActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(EditProfileActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(EditProfileActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(EditProfileActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        getProfile();
    }


    public class StringWithTag {
        public String string;
        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }
    public void updateProfile(){
        if(fname.getText().toString().trim().length() == 0){
            fname.setError("Please enter firstname");
            fname.requestFocus();
            return;
        }
        if(lname.getText().toString().trim().length() == 0){
            lname.setError("Please enter lastname");
            lname.requestFocus();
            return;
        }
        if(address.getText().toString().trim().length() == 0){
            address.setError("Please enter address");
            address.requestFocus();
            return;
        }
        if(coontry_ids.equals("")){
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please choose country", Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
        if(state.getText().toString().trim().length() == 0){
            state.setError("Please enter state");
            state.requestFocus();
            return;
        }
        if(city.getText().toString().trim().length() == 0){
            city.setError("Please enter city");
            city.requestFocus();
            return;
        }
        if(pin.getText().toString().trim().length() == 0){
            pin.setError("Please enter zip code");
            pin.requestFocus();
            return;
        }
        if(phone.getText().toString().trim().length() == 0){
            phone.setError("Please enter phone");
            phone.requestFocus();
            return;
        }
        if(phone.getText().toString().trim().length() < 10){
            phone.setError("Phone number must be 10 digit or more");
            phone.requestFocus();
            return;
        }
        if(mobile.getText().toString().trim().length() == 0){
            mobile.setError("Please enter mobile");
            mobile.requestFocus();
            return;
        }
        if(mobile.getText().toString().trim().length() < 10){
            mobile.setError("Mobile number must be 10 digit or more");
            mobile.requestFocus();
            return;
        }
        if(oldPass.getText().toString().trim().length()>0){
            if(newPass.getText().toString().trim().length() == 0){
                newPass.setError("Please enter new password");
                newPass.requestFocus();
                return;
            }
            if(confPass.getText().toString().trim().length() == 0){
                confPass.setError("Please enter confirm password");
                confPass.requestFocus();
                return;
            }
            if(!newPass.getText().toString().trim().equals(confPass.getText().toString().trim())){
                confPass.setError("Please enter same password again");
                confPass.requestFocus();
                return;
            }
        }
        MethodClass.showProgressDialog(EditProfileActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "update-profile";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        params.put("fname", fname.getText().toString().trim());
        params.put("lname", lname.getText().toString().trim());
        params.put("full_address", address.getText().toString().trim());
        params.put("country_name", coontry_ids);
        params.put("state", state.getText().toString().trim());
        params.put("city", city.getText().toString().trim());
        params.put("zip_code", pin.getText().toString().trim());
        params.put("phone", phone.getText().toString().trim());
        params.put("mobile", mobile.getText().toString().trim());
        if(oldPass.getText().toString().trim().length()>0){
            params.put("old_password", oldPass.getText().toString().trim());
            params.put("password", newPass.getText().toString().trim());
            params.put("password_confirmation", confPass.getText().toString().trim());
        }


        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(EditProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(EditProfileActivity.this, response);
                    if (result_Object != null) {
                        String message = result_Object.getString("message");

                        new SweetAlertDialog(EditProfileActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Profile Updated")
                                .setContentText("Your profile successfully updated")
                                .setConfirmText("Done")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(EditProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(EditProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(EditProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EditProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EditProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(EditProfileActivity.this).addToRequestQueue(jsonObjectRequest);

    }

    public void getProfile(){
        MethodClass.showProgressDialog(EditProfileActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "edit-profile";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", user_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("resp", response.toString());
                try{
                    MethodClass.hideProgressDialog(EditProfileActivity.this);

                    JSONObject result_Object = MethodClass.get_result_from_webservice(EditProfileActivity.this, response);
                    if (result_Object != null) {
                        JSONObject user_details = result_Object.getJSONObject("user_details");

                        String firstName = user_details.getString("fname");

                        if(!firstName.equals("null")){
                            fname.setText(firstName);
                        }

                        String lastName = user_details.getString("lname");

                        if(!lastName.equals("null")){
                            lname.setText(lastName);
                        }

                        String phones = user_details.getString("phone");

                        if(!phones.equals("null")){
                            phone.setText(phones);
                        }

                        String addresss = user_details.getString("full_address");

                        if(!addresss.equals("null")){
                            address.setText(addresss);
                        }


                        String mobiles = user_details.getString("mobile");

                        if(!mobiles.equals("null")){
                            mobile.setText(mobiles);
                        }

                        String emails = user_details.getString("email");

                        if(!emails.equals("null")){
                            email.setText(emails);
                        }
                        String country_id = user_details.getString("country_id");
                        String states = user_details.getString("state");

                        if(!states.equals("null")){
                            state.setText(states);
                        }

                        String citys = user_details.getString("city");

                        if(!citys.equals("null")){
                            city.setText(citys);
                        }

                        String zip_code = user_details.getString("zip_code");

                        if(!zip_code.equals("null")){
                            pin.setText(zip_code);
                        }

                        String country = result_Object.getString("country");

                        JSONArray counArr = new JSONArray(country);
                        List<StringWithTag> spinnerArrays = new ArrayList<StringWithTag>();
                        for (int i = 0; i <counArr.length() ; i++) {
                            String id = counArr.getJSONObject(i).getString("id");
                            String country_name = counArr.getJSONObject(i).getString("country_name");

                            if(id.equals(country_id)){
                                selected = i;
                            }
                            spinnerArrays.add(new StringWithTag(country_name, id));
                        }
                        ArrayAdapter adapter3 = new ArrayAdapter(EditProfileActivity.this, R.layout.multiline_spinner_row, spinnerArrays) {
                        };
                        spinn.setAdapter(adapter3);
                        spinn.setSelection(selected);
                        spinn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            //for user salutation select
                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                // An item was selected. You can retrieve the selected item using
                                //StringWithTag s
                                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                                coontry_ids = String.valueOf(s.tag);
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Another interface callback
                                return;
                            }
                        });

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MethodClass.hideProgressDialog(EditProfileActivity.this);
                    Log.e("error", e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(EditProfileActivity.this);

                Log.e("error", error.toString());
                if (error.toString().contains("AuthFailureError")) {
                    Toast.makeText(EditProfileActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EditProfileActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(EditProfileActivity.this).getString("token", ""));

                Log.e("getHeaders: ", headers.toString());

                return headers;
            }
        };

        MySingleton.getInstance(EditProfileActivity.this).addToRequestQueue(jsonObjectRequest);
    }



    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(this,SignInActivity.class);
            startActivity(I);
        }
    }

    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(EditProfileActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                        Intent I  = new Intent(EditProfileActivity.this,BuyerAddressBookActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(EditProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){

                    }else{
                        Intent I  = new Intent(EditProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(EditProfileActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(EditProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(EditProfileActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(EditProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(EditProfileActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(EditProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(EditProfileActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(EditProfileActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }


    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv;
        ImageView home_img,browse_img,cart_img,login_img;

        LinearLayout login_layout,cart_layout,browse_layout,home_layout;

        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);

        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);


        login_layout=(LinearLayout)findViewById(R.id.login_layout);
        cart_layout=(LinearLayout)findViewById(R.id.cart_layout);
        browse_layout=(LinearLayout)findViewById(R.id.browse_layout);
        home_layout=(LinearLayout)findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }
}
