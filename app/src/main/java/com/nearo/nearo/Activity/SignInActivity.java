package com.nearo.nearo.Activity;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.nearo.nearo.Fragment.LoginFragment;
import com.nearo.nearo.Fragment.RegisterFragment;
import com.nearo.nearo.R;

public class SignInActivity extends AppCompatActivity {
    private Button login,regi;
    private FrameLayout viewpager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        viewpager = (FrameLayout) findViewById(R.id.viewpager);
        login = (Button) findViewById(R.id.login);
        regi = (Button) findViewById(R.id.regi);

        login.setTextColor(getResources().getColor(R.color.white));
        login.setBackground(getDrawable(R.color.menu_back));

        regi.setTextColor(getResources().getColor(R.color.black));
        regi.setBackground(getDrawable(R.color.reg_back));

        Fragment FragmentLogin = getSupportFragmentManager().findFragmentByTag("LOGIN");
        Fragment FragmentReg = getSupportFragmentManager().findFragmentByTag("SIGNUP");

        if (FragmentLogin == null && FragmentReg == null ) {
            addFragment(R.id.viewpager, new LoginFragment(), "LOGIN");
        }

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logi();
            }
        });
        regi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Regi();
            }
        });

    }

    public void Logi(){
        viewpager.setVisibility(View.VISIBLE);
        login.setTextColor(getResources().getColor(R.color.white));
        login.setBackground(getDrawable(R.color.menu_back));

        regi.setTextColor(getResources().getColor(R.color.black));
        regi.setBackground(getDrawable(R.color.reg_back));


        Fragment FragmentLogin = getSupportFragmentManager().findFragmentByTag("LOGIN");
        Fragment FragmentReg = getSupportFragmentManager().findFragmentByTag("SIGNUP");

        if (FragmentLogin == null && FragmentReg == null ) {
            addFragment(R.id.viewpager, new LoginFragment(), "LOGIN");
        }else{
            replaceFragment(R.id.viewpager, new LoginFragment(), "LOGIN");
        }
    }

    public void Regi(){
        viewpager.setVisibility(View.VISIBLE);
        login.setTextColor(getResources().getColor(R.color.black));
        login.setBackground(getDrawable(R.color.reg_back));

        regi.setTextColor(getResources().getColor(R.color.white));
        regi.setBackground(getDrawable(R.color.menu_back));


        Fragment FragmentLogin = getSupportFragmentManager().findFragmentByTag("LOGIN");
        Fragment FragmentReg = getSupportFragmentManager().findFragmentByTag("SIGNUP");

        if (FragmentLogin == null && FragmentReg == null ) {
            addFragment(R.id.viewpager, new RegisterFragment(), "SIGNUP");
        }else{
            replaceFragment(R.id.viewpager, new RegisterFragment(), "SIGNUP");
        }
    }
    protected void addFragment(@IdRes int containerViewId,
                               @NonNull Fragment fragment, @NonNull String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, tag)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment, @NonNull String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, tag)
                .commit();
    }

    public void back(View view){
        super.onBackPressed();
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            //System.out.println("@#@");
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}
