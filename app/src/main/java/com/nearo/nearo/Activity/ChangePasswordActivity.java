package com.nearo.nearo.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordActivity extends AppCompatActivity {
    private EditText newpass,confpass;
    private Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        save = (Button) findViewById(R.id.save);
        newpass = (EditText) findViewById(R.id.newpass);
        confpass = (EditText) findViewById(R.id.confpass);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    }
    public void submit(){
        if (newpass.getText().toString().length() == 0) {
            newpass.setError("New password is required");
            newpass.requestFocus();
            return;
        }
        if (confpass.getText().toString().length() == 0) {
            confpass.setError("Confirm password is required");
            confpass.requestFocus();
            return;
        }
        if(!newpass.getText().toString().equals(confpass.getText().toString())){
            confpass.setError("Confirm password does not match with new password");
            confpass.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(ChangePasswordActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "verify";


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("password", newpass.getText().toString());
        params.put("email", getIntent().getStringExtra("email"));

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ChangePasswordActivity.this, response);
                    if (jsonObject != null) {
                        final String message = jsonObject.getString("message");
                        new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("SUCCESSFUl")
                                .setContentText("Please login using your new password")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent I =new Intent(ChangePasswordActivity.this, SignInActivity.class);
                                        startActivity(I);
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(ChangePasswordActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ChangePasswordActivity.this);
                } else {
                    MethodClass.error_alert(ChangePasswordActivity.this);
                }
            }
        });

        MySingleton.getInstance(ChangePasswordActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void back(View view) {
        onBackPressed();
        Intent i = new Intent(ChangePasswordActivity.this,SignInActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(I);
    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        I.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(I);
    }

}
