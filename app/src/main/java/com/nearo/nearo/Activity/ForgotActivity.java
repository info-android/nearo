package com.nearo.nearo.Activity;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nearo.nearo.Helper.Constants.DEVICE_ID;
import static com.nearo.nearo.Helper.Constants.FNAME;
import static com.nearo.nearo.Helper.Constants.LNAME;
import static com.nearo.nearo.Helper.Constants.PROVIDER_ID;

public class ForgotActivity extends AppCompatActivity {
    private EditText mobile;
    private Button reset;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        mobile = (EditText) findViewById(R.id.mobile);
        reset = (Button) findViewById(R.id.reset);



    }
    public void submit(){
        if(!emailValidator(mobile.getText().toString().trim())){
            mobile.setError("Please enter valid email");
            mobile.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(ForgotActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "forgot-password";


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", mobile.getText().toString().trim());

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ForgotActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ForgotActivity.this, response);
                    if (jsonObject != null) {
                        JSONObject jsonObject1 = new JSONObject(jsonObject.getString("user_details"));
                        String vcode="";
                        if (jsonObject1.has("vcode")){
                            vcode = jsonObject1.getString("vcode");
                        }
                        Intent intent = new Intent(ForgotActivity.this, Verify2Activity.class);
                        intent.putExtra("email", mobile.getText().toString().trim());
                        intent.putExtra("vcode", vcode);
                        startActivity(intent);


                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(ForgotActivity.this);
                    Log.e("EXEC", e.toString() );
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("EXEC", error.toString() );
                MethodClass.hideProgressDialog(ForgotActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ForgotActivity.this);
                } else {
                    MethodClass.error_alert(ForgotActivity.this);
                }
            }
        });

        MySingleton.getInstance(ForgotActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    public void reset(View view){
        submit();
    }
    public void back(View view){
        super.onBackPressed();
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }

}
