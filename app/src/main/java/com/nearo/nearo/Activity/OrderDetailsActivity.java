package com.nearo.nearo.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.CheckoutAdapter;
import com.nearo.nearo.Adapter.OrderDetailAdapter;
import com.nearo.nearo.Adapter.OrderListAdapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.CART_ID;
import static com.nearo.nearo.Helper.Constants.ORD_DATE;
import static com.nearo.nearo.Helper.Constants.ORD_DIS;
import static com.nearo.nearo.Helper.Constants.ORD_ID;
import static com.nearo.nearo.Helper.Constants.ORD_NO;
import static com.nearo.nearo.Helper.Constants.ORD_PAY;
import static com.nearo.nearo.Helper.Constants.ORD_QTY;
import static com.nearo.nearo.Helper.Constants.ORD_STAT;
import static com.nearo.nearo.Helper.Constants.ORD_SUB;
import static com.nearo.nearo.Helper.Constants.ORD_TOTAL;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.PRO_RECIE;
import static com.nearo.nearo.Helper.Constants.PRO_REV;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;
import static com.nearo.nearo.Helper.Constants.USER_SELECTED_HEALTH;

public class OrderDetailsActivity extends AppCompatActivity {
    private RecyclerView recycler_view;
    private ArrayList<HashMap<String, String>> orderlist=new ArrayList<HashMap<String, String>>();
    private Boolean is_logged_in = false;
    private TextView placeName;
    private EditText searchItem;
    private String user_id;
    private String ord_m_id;
    private Button markRecieve;

    private TextView ord_number;
    private TextView ord_Cre;
    private TextView ord_del;
    private LinearLayout delCOn;
    private TextView fname;
    private TextView shipAdd;
    private TextView shipPhone;
    private TextView shipEmail;
    private TextView ord_qty;
    private TextView ord_total;
    private NestedScrollView scrollView;

    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        recycler_view = findViewById(R.id.recycler_view);
        placeName = findViewById(R.id.placeName);
        searchItem  = findViewById(R.id.searchItem);

        user_id = PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getString("user_id","");
        bottumLaySetColor("more");
        MethodClass.placeName(OrderDetailsActivity.this,placeName);
        MethodClass.searchKey(OrderDetailsActivity.this,searchItem);

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getBoolean("is_logged_in",false);
        USER_SELECTED_HEALTH =new  ArrayList<Integer>();
        ord_number  = findViewById(R.id.ord_number);
        ord_Cre  = findViewById(R.id.ord_Cre);
        ord_del  = findViewById(R.id.ord_del);
        delCOn  = findViewById(R.id.delCOn);
        fname  = findViewById(R.id.fname);
        shipAdd  = findViewById(R.id.shipAdd);
        shipPhone  = findViewById(R.id.shipPhone);
        shipEmail  = findViewById(R.id.shipEmail);
        ord_qty  = findViewById(R.id.ord_qty);
        ord_total  = findViewById(R.id.ord_total);
        markRecieve  = findViewById(R.id.markRecieve);
        scrollView  = findViewById(R.id.scrollView);

        markRecieve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mark_recieve();
            }
        });

        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(OrderDetailsActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(OrderDetailsActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(OrderDetailsActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(OrderDetailsActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }
    public void mark_recieve(){
        if(USER_SELECTED_HEALTH.size()>0){
            if (MethodClass.isNetworkConnected(OrderDetailsActivity.this)) {
                MethodClass.showProgressDialog(OrderDetailsActivity.this);
                String server_url = getString(R.string.SERVER_URL) + "product-deliver";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("product_delivered_id", TextUtils.join(",",USER_SELECTED_HEALTH));
                params.put("order_master_id", ord_m_id);
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("HomeRes", response.toString());

                        try {
                            MethodClass.hideProgressDialog(OrderDetailsActivity.this);

                            JSONObject result_Object = MethodClass.get_result_from_webservice(OrderDetailsActivity.this, response);
                            if (result_Object != null) {
                                String mess = result_Object.getString("message");
                                getList();
                                new SweetAlertDialog(OrderDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Successful")
                                        .setContentText("Order marked as received")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR", error.toString());
                        MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                        if (error.toString().contains("ConnectException")) {
                            MethodClass.network_error_alert(OrderDetailsActivity.this);
                        } else {
                            MethodClass.error_alert(OrderDetailsActivity.this);
                        }

                    }
                }){
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        if(!PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getString("token", "").equals("")){
                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getString("token", ""));
                        }


                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Access the RequestQueue through your singleton class.
                MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
            } else {
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                snackbar.show();
                return;
            }
        }else{
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please check product(s) first", Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getList() {
        if (MethodClass.isNetworkConnected(OrderDetailsActivity.this)) {
            MethodClass.showProgressDialog(OrderDetailsActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "order-details";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("order_number",getIntent().getStringExtra("ord_no"));
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(OrderDetailsActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(OrderDetailsActivity.this, response);
                        if (result_Object != null) {
                            String order = result_Object.getString("order");

                            JSONObject ordObj = new JSONObject(order);
                            ord_number.setText("ORDER ID #"+ordObj.getString("order_number"));
                            if(ordObj.getString("order_status").equals("D")){
                                delCOn.setVisibility(View.VISIBLE);
                                ord_del.setText(ordObj.getString("updated_at"));
                            }else {
                                delCOn.setVisibility(View.GONE);
                            }
                            ord_Cre.setText(ordObj.getString("order_date"));
                            ord_qty.setText("Total quantity: "+ordObj.getString("order_qty"));
                            ord_total.setText("₹"+ordObj.getString("order_total"));
                            fname.setText(ordObj.getString("shipping_fname")+" "+ordObj.getString("shipping_lname"));
                            shipAdd.setText(ordObj.getString("shipping_address"));
                            shipEmail.setText(ordObj.getString("shipping_email"));
                            shipPhone.setText(ordObj.getString("shipping_phone"));


                            String my_order = result_Object.getString("details");
                            orderlist=new ArrayList<HashMap<String, String>>();
                            if(!my_order.equals("null") && !my_order.equals(null)){

                                JSONArray myOrdArr = new JSONArray(my_order);
                                if (myOrdArr.length() != 0) {

                                    Log.e("featured_jsonArray", String.valueOf(myOrdArr.length()));
                                    for (int i = 0; i < myOrdArr.length(); i++) {

                                        HashMap<String, String> map = new HashMap<String, String>();

                                        JSONObject productsObj =myOrdArr.getJSONObject(i);
                                        String id = productsObj.getString("id");
                                        String order_master_id = productsObj.getString("order_master_id");
                                        String product_id = productsObj.getString("product_id");
                                        String reviewed_by_buyer = productsObj.getString("reviewed_by_buyer");
                                        ord_m_id = order_master_id;
                                        String total_price = productsObj.getString("total_price");
                                        String customer_received = productsObj.getString("customer_received");
                                        JSONObject product = productsObj.getJSONObject("product");

                                        String name = product.getString("name");
                                        String description = product.getString("description");
                                        JSONObject imgObj = product.getJSONObject("img");
                                        String img_name = imgObj.getString("image");



                                        map.put(PRO_ID,id);
                                        map.put(CART_ID,product_id);
                                        map.put(PRO_NAME,name);
                                        map.put(PRO_DESC,description);
                                        map.put(PRO_IMG,img_name);
                                        map.put(PRO_PRICE,total_price);
                                        map.put(PRO_RECIE,customer_received);
                                        map.put(PRO_REV,reviewed_by_buyer);

                                        orderlist.add(map);

                                    }
                                    OrderDetailAdapter adapter = new OrderDetailAdapter(OrderDetailsActivity.this, orderlist);
                                    recycler_view.setAdapter(adapter);
                                }

                            }
                            scrollView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(OrderDetailsActivity.this);
                    } else {
                        MethodClass.error_alert(OrderDetailsActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getList();

    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(OrderDetailsActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                        Intent I  = new Intent(OrderDetailsActivity.this,BuyerAddressBookActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(OrderDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){
                        Intent I  = new Intent(OrderDetailsActivity.this,EditProfileActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(OrderDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(OrderDetailsActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(OrderDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(OrderDetailsActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(OrderDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(OrderDetailsActivity.this,OrderListActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(OrderDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(OrderDetailsActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(OrderDetailsActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(OrderDetailsActivity.this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(OrderDetailsActivity.this,SignInActivity.class);
            startActivity(I);
        }
    }
    private void bottumLaySetColor(String s) {
        TextView home_tv, browse_tv, cart_tv, login_tv;
        ImageView home_img, browse_img, cart_img, login_img;

        LinearLayout login_layout, cart_layout, browse_layout, home_layout;

        home_tv = (TextView) findViewById(R.id.home_tv);
        browse_tv = (TextView) findViewById(R.id.browse_tv);
        cart_tv = (TextView) findViewById(R.id.cart_tv);
        login_tv = (TextView) findViewById(R.id.login_tv);

        home_img = (ImageView) findViewById(R.id.home_img);
        browse_img = (ImageView) findViewById(R.id.browse_img);
        cart_img = (ImageView) findViewById(R.id.cart_img);
        login_img = (ImageView) findViewById(R.id.login_img);


        login_layout = (LinearLayout) findViewById(R.id.login_layout);
        cart_layout = (LinearLayout) findViewById(R.id.cart_layout);
        browse_layout = (LinearLayout) findViewById(R.id.browse_layout);
        home_layout = (LinearLayout) findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")) {
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")) {
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")) {
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")) {
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }
}
