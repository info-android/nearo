package com.nearo.nearo.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.CartAdapter;
import com.nearo.nearo.Adapter.CheckoutAdapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.CART_ID;
import static com.nearo.nearo.Helper.Constants.CART_M_ID;
import static com.nearo.nearo.Helper.Constants.CART_NAME;
import static com.nearo.nearo.Helper.Constants.CART_QUANTITY;
import static com.nearo.nearo.Helper.Constants.CART_SELLER;
import static com.nearo.nearo.Helper.Constants.CART_SUBTOTAL;
import static com.nearo.nearo.Helper.Constants.CART_TOTAL;
import static com.nearo.nearo.Helper.Constants.CART_UNIT;
import static com.nearo.nearo.Helper.Constants.DEVICE_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class CheckoutActivity extends AppCompatActivity {
    private RecyclerView recycler_view;
    private ArrayList<HashMap<String, String>> orderlist;
    private Boolean is_logged_in = false;
    private TextView placeName;
    private TextView del_Add;
    private EditText searchItem;
    private String user_id;
    private String coup_id;
    private String c_m_id;
    private int add_id = 0;
    private RadioGroup addGrp;

    private TextView subCount;
    private TextView discount;
    private TextView total;
    private TextView qty;
    private TextView cdiscount;
    private EditText couponTxt;


    private Button confirm;
    private Button coupApp;
    private LinearLayout cDis;
    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        placeName = findViewById(R.id.placeName);
        searchItem  = findViewById(R.id.searchItem);
        addGrp  = findViewById(R.id.addGrp);
        cDis  = findViewById(R.id.cDis);
        user_id = PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("user_id","");
        bottumLaySetColor("mores");
        MethodClass.placeName(CheckoutActivity.this,placeName);
        MethodClass.searchKey(CheckoutActivity.this,searchItem);

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getBoolean("is_logged_in",false);
        recycler_view = findViewById(R.id.recycler_view);

        subCount  = findViewById(R.id.subCount);
        del_Add  = findViewById(R.id.del_Add);
        total  = findViewById(R.id.total);
        discount  = findViewById(R.id.discount);
        qty  = findViewById(R.id.qty);
        confirm  = findViewById(R.id.confirm);
        coupApp  = findViewById(R.id.coupApp);
        couponTxt  = findViewById(R.id.couponTxt);
        cdiscount  = findViewById(R.id.cdiscount);

        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(CheckoutActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(CheckoutActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(CheckoutActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(CheckoutActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDel();
            }
        });
        coupApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyCoupon();
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }
    public void applyCoupon(){
        if (MethodClass.isNetworkConnected(CheckoutActivity.this)) {
            if(couponTxt.getText().toString().trim().length() == 0){
                couponTxt.setError("Enter coupon code first");
                couponTxt.requestFocus();
                return;
            }
            MethodClass.showProgressDialog(CheckoutActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "check-coupon";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("coupon_code",couponTxt.getText().toString().trim());


            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(CheckoutActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(CheckoutActivity.this, response);
                        if (result_Object != null) {

                            String coupon_id = result_Object.getString("coupon_id");
                            String coupon_discount = result_Object.getString("coupon_discount");
                            String totalss = result_Object.getString("total");
                            cdiscount.setText("₹"+coupon_discount);
                            total.setText("₹"+totalss);
                            coup_id = coupon_id;
                            cDis.setVisibility(View.VISIBLE);
                            new SweetAlertDialog(CheckoutActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Coupon Applied")
                                    .setContentText("Coupon successfully applied")
                                    .setConfirmText("Okay")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(CheckoutActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(CheckoutActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(CheckoutActivity.this);
                    } else {
                        MethodClass.error_alert(CheckoutActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void confirmDel(){
        if (MethodClass.isNetworkConnected(CheckoutActivity.this)) {
            if(add_id == 0){
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please choose address first", Snackbar.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            MethodClass.showProgressDialog(CheckoutActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "check-location-order";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("cart_master_id",c_m_id);
            params.put("address_id",String.valueOf(add_id));

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(CheckoutActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(CheckoutActivity.this, response);
                        if (result_Object != null) {

                            String message = result_Object.getString("message");

                            Log.e("Messahe", message );
                            if(message.equals("Success.")){

                                MethodClass.showProgressDialog(CheckoutActivity.this);
                                String server_url = getString(R.string.SERVER_URL) + "confirm-order";
                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("payment_method","COD");
                                params.put("address_id",String.valueOf(add_id));
                                params.put("coupon_code",couponTxt.getText().toString().trim());

                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("HomeRes", response.toString());

                                        try {
                                            MethodClass.hideProgressDialog(CheckoutActivity.this);

                                            JSONObject result_Object = MethodClass.get_result_from_webservice(CheckoutActivity.this, response);
                                            if (result_Object != null) {

                                                String message = result_Object.getString("message");
                                                new SweetAlertDialog(CheckoutActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Order Placed")
                                                        .setContentText("Order successfully placed please click okay to view the order")
                                                        .setConfirmText("Okay")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                                Intent I = new Intent(CheckoutActivity.this,OrderListActivity.class);
                                                                I.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                startActivity(I);
                                                                finish();
                                                            }
                                                        })
                                                        .show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            MethodClass.hideProgressDialog(CheckoutActivity.this);
                                            Log.e("error", e.getMessage());
                                        }


                                    }
                                }, new Response.ErrorListener() {

                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e("ERROR", error.toString());
                                        MethodClass.hideProgressDialog(CheckoutActivity.this);
                                        if (error.toString().contains("ConnectException")) {
                                            MethodClass.network_error_alert(CheckoutActivity.this);
                                        } else {
                                            MethodClass.error_alert(CheckoutActivity.this);
                                        }

                                    }
                                }){
                                    //* Passing some request headers*
                                    @Override
                                    public Map getHeaders() throws AuthFailureError {
                                        HashMap headers = new HashMap();
                                        headers.put("Content-Type", "application/json");
                                        if(!PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", "").equals("")){
                                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", ""));
                                        }


                                        Log.e("getHeaders: ", headers.toString());

                                        return headers;
                                    }
                                };
                                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                // Access the RequestQueue through your singleton class.
                                MySingleton.getInstance(CheckoutActivity.this).addToRequestQueue(jsObjRequest);

                            }else{
                                String showing_message = result_Object.getString("showing_message");
                                new SweetAlertDialog(CheckoutActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText(message)
                                        .setContentText(String.valueOf(Html.fromHtml(showing_message)))
                                        .setConfirmText("Okay")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(CheckoutActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(CheckoutActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(CheckoutActivity.this);
                    } else {
                        MethodClass.error_alert(CheckoutActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getList() {

        if (MethodClass.isNetworkConnected(CheckoutActivity.this)) {
            MethodClass.showProgressDialog(CheckoutActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "check-out";
            HashMap<String, String> params = new HashMap<String, String>();
            if(is_logged_in){
                params.put("user_id",user_id);
            }else {
                params.put("divice_id",DEVICE_ID);
            }

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(CheckoutActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(CheckoutActivity.this, response);
                        if (result_Object != null) {

                            String shipping_address = result_Object.getString("shipping_address");
                            String cartM = result_Object.getString("cart");
                            if(!shipping_address.equals("null") && !shipping_address.equals(null)){
                                JSONArray shiArr = new JSONArray(shipping_address);
                                if (shiArr.length() != 0) {

                                    Log.e("featured_jsonArray", String.valueOf(shiArr.length()));
                                    for (int i = 0; i < shiArr.length(); i++) {

                                        JSONObject productsObj =shiArr.getJSONObject(i);
                                        String id = productsObj.getString("id");
                                        String location = productsObj.getString("location");
                                        String lat = productsObj.getString("lat");
                                        String lng = productsObj.getString("lng");
                                        String zip_code = productsObj.getString("zip_code");
                                        JSONObject country_name = productsObj.getJSONObject("country_name");
                                        String name =country_name.getString("country_name");

                                        final RadioButton rdbtn = new RadioButton(CheckoutActivity.this);
                                        rdbtn.setId(Integer.parseInt(id));
                                        rdbtn.setTag(location+"\nPin: "+zip_code+"\nCountry: "+name+"\n");
                                        rdbtn.setText(location+"\nPin: "+zip_code+"\nCountry: "+name+"\n");
                                        rdbtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                add_id = rdbtn.getId();
                                                del_Add.setText(String.valueOf(rdbtn.getTag()));
                                            }
                                        });
                                        addGrp.addView(rdbtn);


                                    }
                                }
                            }

                            orderlist=new ArrayList<HashMap<String, String>>();
                            if(!cartM.equals("null") && !cartM.equals(null)){

                                JSONObject cartMaster = new JSONObject(cartM);
                                subCount.setText(" ₹"+cartMaster.getString("subtotal"));
                                discount.setText("₹"+cartMaster.getString("discount"));
                                total.setText("₹"+cartMaster.getString("total_price"));
                                qty.setText(cartMaster.getString("total_qty"));

                                JSONArray cart = cartMaster.getJSONArray("cart");
                                if (cart.length() != 0) {

                                    Log.e("featured_jsonArray", String.valueOf(cart.length()));
                                    for (int i = 0; i < cart.length(); i++) {

                                        HashMap<String, String> map = new HashMap<String, String>();

                                        JSONObject productsObj =cart.getJSONObject(i);
                                        String id = productsObj.getString("id");
                                        String cart_master_id = productsObj.getString("cart_master_id");
                                        String qty = productsObj.getString("qty");
                                        String total_price = productsObj.getString("total_price");
                                        String subtotal = productsObj.getString("subtotal");
                                        JSONObject product = productsObj.getJSONObject("product");
                                        String name =product.getString("name");
                                        String starting_price =product.getString("starting_price");
                                        JSONObject img =product.getJSONObject("img");
                                        String imgName = img.getString("image");

                                        c_m_id = cart_master_id;
                                        JSONObject seller_name = productsObj.getJSONObject("seller_name");
                                        String fname =seller_name.getString("fname");
                                        String lname =seller_name.getString("lname");

                                        map.put(CART_ID,id);
                                        map.put(CART_M_ID,cart_master_id);
                                        map.put(CART_QUANTITY,qty);
                                        map.put(CART_TOTAL,total_price);
                                        map.put(CART_SUBTOTAL,subtotal);
                                        map.put(CART_SELLER,fname+" "+lname);
                                        map.put(CART_UNIT,starting_price);
                                        map.put(CART_NAME,name);
                                        map.put(PRO_IMG,imgName);
                                        orderlist.add(map);

                                    }
                                    CheckoutAdapter adapter = new CheckoutAdapter(CheckoutActivity.this, orderlist);
                                    recycler_view.setAdapter(adapter);
                                }

                            }else {

                            }


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(CheckoutActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(CheckoutActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(CheckoutActivity.this);
                    } else {
                        MethodClass.error_alert(CheckoutActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CheckoutActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getList();
    }

    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void AddAddress(View view){
        Intent I  = new Intent(this,BuyerAddressBookActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(CheckoutActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                        Intent I  = new Intent(CheckoutActivity.this,BuyerAddressBookActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(CheckoutActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){
                        Intent I  = new Intent(CheckoutActivity.this,EditProfileActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(CheckoutActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(CheckoutActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(CheckoutActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(CheckoutActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(CheckoutActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(CheckoutActivity.this,OrderListActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(CheckoutActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(CheckoutActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(CheckoutActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(CheckoutActivity.this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(CheckoutActivity.this,SignInActivity.class);
            startActivity(I);
        }
    }
    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv;
        ImageView home_img,browse_img,cart_img,login_img;

        LinearLayout login_layout,cart_layout,browse_layout,home_layout;

        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);

        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);


        login_layout=(LinearLayout)findViewById(R.id.login_layout);
        cart_layout=(LinearLayout)findViewById(R.id.cart_layout);
        browse_layout=(LinearLayout)findViewById(R.id.browse_layout);
        home_layout=(LinearLayout)findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }
}
