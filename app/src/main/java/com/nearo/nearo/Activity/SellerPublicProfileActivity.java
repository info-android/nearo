package com.nearo.nearo.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.FavouriteAdapter;
import com.nearo.nearo.Adapter.Home1Adapter;
import com.nearo.nearo.Adapter.Home2Adapter;
import com.nearo.nearo.Adapter.Home3Adapter;
import com.nearo.nearo.Adapter.ReviewAdapter;
import com.nearo.nearo.Adapter.SellerProductAdapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.COM_IMG_URL;
import static com.nearo.nearo.Helper.Constants.FNAME;
import static com.nearo.nearo.Helper.Constants.LNAME;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_DISC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.REV_COMM;
import static com.nearo.nearo.Helper.Constants.REV_DATE;
import static com.nearo.nearo.Helper.Constants.REV_HEAD;
import static com.nearo.nearo.Helper.Constants.REV_POINT;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class SellerPublicProfileActivity extends AppCompatActivity {
    private RecyclerView recycler_view,recycler_view1,recycler_view2;

    private ArrayList<HashMap<String, String>> orderlist=new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>>orderlist1=new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>>orderlist2=new ArrayList<HashMap<String, String>>();


    private Boolean is_logged_in = false;
    private TextView placeName;

    private TextView comAbt,comMain,comLoca,comEmp,companyName,sellerTitle,order,products,memberSince;

    private ScaleRatingBar comRatingBar;
    private ImageView comImg;

    private NestedScrollView scrollView;
    private LinearLayout rev_contain;

    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_public_profile);

        recycler_view  = findViewById(R.id.recycler_view);
        recycler_view1  = findViewById(R.id.recycler_view1);
        recycler_view2  = findViewById(R.id.recycler_view2);
        placeName = findViewById(R.id.placeName);

        comAbt = findViewById(R.id.comAbt);
        comMain = findViewById(R.id.comMain);
        comLoca = findViewById(R.id.comLoca);
        sellerTitle = findViewById(R.id.sellerTitle);
        order = findViewById(R.id.order);
        products = findViewById(R.id.products);
        memberSince = findViewById(R.id.memberSince);
        comImg = findViewById(R.id.comImg);
        comRatingBar = findViewById(R.id.comRatingBar);
        scrollView = findViewById(R.id.scrollView);
        rev_contain = findViewById(R.id.rev_contain);


        bottumLaySetColor("mores");
        MethodClass.placeName(SellerPublicProfileActivity.this,placeName);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(SellerPublicProfileActivity.this).getBoolean("is_logged_in",false);
        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(SellerPublicProfileActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(SellerPublicProfileActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(SellerPublicProfileActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(SellerPublicProfileActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();

                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        getProfile();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }
    public void getProfile(){

        if (MethodClass.isNetworkConnected(SellerPublicProfileActivity.this)) {
            MethodClass.showProgressDialog(SellerPublicProfileActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "seller-public-profile";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("seller_id", getIntent().getStringExtra("com_id"));

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(SellerPublicProfileActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(SellerPublicProfileActivity.this, response);
                        if (result_Object != null) {

                            JSONObject sellerObj = result_Object.getJSONObject("seller");

                            String fname = sellerObj.getString("fname");
                            String lname = sellerObj.getString("lname");
                            String profile_pic = sellerObj.getString("profile_pic");
                            String company_name = sellerObj.getString("company_name");
                            String company_description = sellerObj.getString("company_description");
                            String full_address = sellerObj.getString("full_address");
                            String avg_review = sellerObj.getString("avg_review");
                            String created_at = sellerObj.getString("created_at");

                            sellerTitle.setText(company_name);

                            if(!company_description.equals("null") && !company_description.equals(null)){
                                comAbt.setText(Html.fromHtml(company_description));
                            }else {
                                comAbt.setText("");
                            }


                            comRatingBar.setRating(Float.parseFloat(avg_review));

                            comLoca.setText(full_address);
                            Picasso.get().load(COM_IMG_URL+profile_pic).placeholder(R.drawable.home_13).error(R.drawable.home_13).into(comImg);
                            memberSince.setText(created_at);




                            String orderss = result_Object.getString("order");
                            order.setText(orderss);

                            JSONArray productsArr = sellerObj.getJSONArray("products");

                            if (productsArr.length() != 0) {

                                products.setText(String.valueOf(productsArr.length()));

                                List<String> list = new ArrayList<>();
                                for (int i = 0; i < productsArr.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    JSONObject productsObj = productsArr.getJSONObject(i);
                                    String name = productsObj.getString("name");
                                    list.add(name);
                                }
                                comMain.setText(TextUtils.join(", ", list));
                            }else {
                                products.setText("");
                                comMain.setText("");
                            }
                            JSONArray productsArr2 = result_Object.getJSONArray("product");

                            if (productsArr2.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(productsArr2.length()));
                                for (int i = 0; i < productsArr2.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    JSONObject productsObj2 = productsArr2.getJSONObject(i);
                                    String id = productsObj2.getString("id");
                                    String name = productsObj2.getString("name");
                                    String description = productsObj2.getString("description");
                                    String starting_price =productsObj2.getString("starting_price");
                                    String starting_disc_price =productsObj2.getString("starting_disc_price");

                                    String img = productsObj2.getString("img");

                                    JSONObject imgObj = new JSONObject(img);

                                    map.put(PRO_ID,id);
                                    map.put(PRO_NAME,name);
                                    map.put(PRO_DESC,description);
                                    map.put(PRO_PRICE,starting_price);
                                    map.put(PRO_DISC,starting_disc_price);
                                    map.put(PRO_IMG,imgObj.getString("image"));
                                    orderlist.add(map);

                                }
                                SellerProductAdapter adapter = new SellerProductAdapter(SellerPublicProfileActivity.this, orderlist);
                                recycler_view.setAdapter(adapter);
                            }
                            JSONArray review = result_Object.getJSONArray("review");
                            if (review.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(productsArr.length()));
                                for (int i = 0; i < review.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    JSONObject reviewObj = review.getJSONObject(i);
                                    String review_point = reviewObj.getString("review_point");
                                    String review_heading = reviewObj.getString("review_heading");
                                    String review_comment = reviewObj.getString("review_comment");
                                    String createds_at =reviewObj.getString("created_at");

                                    String user_name = reviewObj.getString("user_name");

                                    JSONObject user_nameObj = new JSONObject(user_name);

                                    String fnames = user_nameObj.getString("fname");
                                    String lnames = user_nameObj.getString("lname");
                                    String profile_pice = user_nameObj.getString("profile_pic");


                                    map.put(REV_COMM,review_comment);
                                    map.put(REV_DATE,createds_at);
                                    map.put(REV_HEAD,review_heading);
                                    map.put(REV_POINT,review_point);


                                    map.put(PRO_IMG,profile_pice);
                                    map.put(FNAME,fnames);
                                    map.put(LNAME,lnames);
                                    orderlist2.add(map);

                                }
                                ReviewAdapter adapter = new ReviewAdapter(SellerPublicProfileActivity.this, orderlist2);
                                recycler_view2.setAdapter(adapter);
                            }else {
                                rev_contain.setVisibility(View.GONE);
                            }
                            for (int i = 0; i <10 ; i++) {
                                HashMap<String,String> map=new HashMap<String, String>();
                                map.put("TEST","Test");
                                orderlist1.add(map);
                            }
                            Home2Adapter adapter1 = new Home2Adapter(SellerPublicProfileActivity.this,orderlist1);
                            recycler_view1.setAdapter(adapter1);

                            scrollView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(SellerPublicProfileActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(SellerPublicProfileActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(SellerPublicProfileActivity.this);
                    } else {
                        MethodClass.error_alert(SellerPublicProfileActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(SellerPublicProfileActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SellerPublicProfileActivity.this).getString("token", ""));
                    }
                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

    }

    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(SellerPublicProfileActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                        Intent I  = new Intent(SellerPublicProfileActivity.this,BuyerAddressBookActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SellerPublicProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){
                        Intent I  = new Intent(SellerPublicProfileActivity.this,EditProfileActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SellerPublicProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(SellerPublicProfileActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SellerPublicProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(SellerPublicProfileActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SellerPublicProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(SellerPublicProfileActivity.this,OrderListActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SellerPublicProfileActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(SellerPublicProfileActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(SellerPublicProfileActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(SellerPublicProfileActivity.this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(SellerPublicProfileActivity.this,SignInActivity.class);
            startActivity(I);
        }
    }
    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv;
        ImageView home_img,browse_img,cart_img,login_img;

        LinearLayout login_layout,cart_layout,browse_layout,home_layout;

        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);

        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);


        login_layout=(LinearLayout)findViewById(R.id.login_layout);
        cart_layout=(LinearLayout)findViewById(R.id.cart_layout);
        browse_layout=(LinearLayout)findViewById(R.id.browse_layout);
        home_layout=(LinearLayout)findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }
}
