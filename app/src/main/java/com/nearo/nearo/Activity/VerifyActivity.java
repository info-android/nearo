package com.nearo.nearo.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class VerifyActivity extends AppCompatActivity {
    private Button verify;
    private TextView otp;
    private EditText editText1,editText2,editText3,editText4;
    private String edtxt11,edtxt12,edtxt13,edtxt14;
    private TextView resend_tv;
    private String code="",email = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        verify = findViewById(R.id.verify);
        otp = findViewById(R.id.otp);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        resend_tv = (TextView) findViewById(R.id.resend_tv);

        if (getIntent().getExtras() != null) {
            code = getIntent().getStringExtra("vcode");
            email = getIntent().getStringExtra("email");
            otp.setText("Your OTP is : " + code);
        }


        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText2.requestFocus();
                } else {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText3.requestFocus();
                } else {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    editText4.requestFocus();
                } else {
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });
        editText4.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    if (!editText1.getText().toString().equals("") && !editText2.getText().toString().equals("") && !editText3.getText().toString().equals("") && !editText4.getText().toString().equals("")) {
                        Log.e("editText1", editText1.getText().toString());
                        Log.e("editText1", editText2.getText().toString());
                        Log.e("editText1", editText3.getText().toString());
                        Log.e("editText1", editText4.getText().toString());
                        verification();
                    } else {
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please enter all the four digits in order to continue", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } else {
                    //editText5.setBackground(getDrawable(R.drawable.verify_grey_edit_back));
                }

            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

        });


        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verification();
            }
        });
        resend_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resend_otp();
            }
        });
    }

    private void verification() {
        if(editText1.getText().toString().trim().length() == 0 || editText2.getText().toString().trim().length() == 0 || editText3.getText().toString().trim().length() == 0 || editText4.getText().toString().trim().length()==0){
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Entered verification code format is invalid", Snackbar.LENGTH_LONG);
            snackbar.show();
            return;
        }
        edtxt11 = editText1.getText().toString();
        edtxt12 = editText2.getText().toString();
        edtxt13 = editText3.getText().toString();
        edtxt14 = editText4.getText().toString();
        String Vcode = (edtxt11 +""+ edtxt12 +""+ edtxt13 +""+ edtxt14);
        MethodClass.showProgressDialog(VerifyActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "verify";


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("vcode", Vcode);
        params.put("email", email);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(VerifyActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(VerifyActivity.this, response);
                    if (jsonObject != null) {
                        final String message = jsonObject.getString("message");
                        new SweetAlertDialog(VerifyActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("VERIFICATION SUCCESSFUL")
                                .setContentText("Please login to use our app")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        Intent I =new Intent(VerifyActivity.this, SignInActivity.class);
                                        startActivity(I);
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(VerifyActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(VerifyActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(VerifyActivity.this);
                } else {
                    MethodClass.error_alert(VerifyActivity.this);
                }
            }
        });

        MySingleton.getInstance(VerifyActivity.this).addToRequestQueue(jsonObjectRequest);
    }



    private void resend_otp(){
        MethodClass.showProgressDialog(VerifyActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "resend-vcode";


        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email);

        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(VerifyActivity.this);
                Log.e("resp", response.toString());
                try {

                    JSONObject jsonObject = MethodClass.get_result_from_webservice(VerifyActivity.this, response);
                    if (jsonObject != null) {
                        final String vcode = jsonObject.getString("vcode");

                        otp.setText("Your OTP is : " + vcode);
                        new SweetAlertDialog(VerifyActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("OTP SENT")
                                .setContentText("Otp sent to your registered email")
                                .setConfirmText("Okay")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    MethodClass.error_alert(VerifyActivity.this);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MethodClass.hideProgressDialog(VerifyActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(VerifyActivity.this);
                } else {
                    MethodClass.error_alert(VerifyActivity.this);
                }
            }
        });

        MySingleton.getInstance(VerifyActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }

    @Override
    public void onBackPressed() {
        String et1="",et2="",et3="",et4="";

        et1=editText1.getText().toString().trim();
        et2=editText2.getText().toString().trim();
        et3=editText3.getText().toString().trim();
        et4=editText4.getText().toString().trim();

        if (!et4.equals("")){
            editText4.setText("");
            editText3.requestFocus();
            return;
        }
        if (!et3.equals("")){
            editText3.setText("");
            editText2.requestFocus();
            return;
        }
        if (!et2.equals("")){
            editText2.setText("");
            editText1.requestFocus();
            return;
        }
        if (!et1.equals("")){
            editText1.setText("");
            editText1.requestFocus();
            return;
        }
        super.onBackPressed();
    }

    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
}
