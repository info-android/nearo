package com.nearo.nearo.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.Home1Adapter;
import com.nearo.nearo.Adapter.Home2Adapter;
import com.nearo.nearo.Adapter.Home3Adapter;
import com.nearo.nearo.Adapter.Home4Adapter;
import com.nearo.nearo.Adapter.Home5Adapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import static com.nearo.nearo.Helper.Constants.BAN_IMG_URL;
import static com.nearo.nearo.Helper.Constants.CAT_ID;
import static com.nearo.nearo.Helper.Constants.CAT_IMG;
import static com.nearo.nearo.Helper.Constants.CAT_NAME;
import static com.nearo.nearo.Helper.Constants.COM_ADD;
import static com.nearo.nearo.Helper.Constants.COM_ID;
import static com.nearo.nearo.Helper.Constants.COM_IMG;
import static com.nearo.nearo.Helper.Constants.COM_NAME;
import static com.nearo.nearo.Helper.Constants.COM_RATE;
import static com.nearo.nearo.Helper.Constants.IS_FAV;
import static com.nearo.nearo.Helper.Constants.PICK_LANG;
import static com.nearo.nearo.Helper.Constants.PICK_LAT;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_DISC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.SLIDER_HEAD1;
import static com.nearo.nearo.Helper.Constants.SLIDER_HEAD2;
import static com.nearo.nearo.Helper.Constants.SLIDER_HEAD3;
import static com.nearo.nearo.Helper.Constants.SLIDER_ID;
import static com.nearo.nearo.Helper.Constants.SLIDER_IMG;
import static com.nearo.nearo.Helper.Constants.SLIDER_URL;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recycler_view, recycler_view1, recycler_view2, recycler_view3, recycler_view4;
    private ArrayList<HashMap<String, String>> orderlist;
    private ArrayList<HashMap<String, String>> orderlist1;
    private ArrayList<HashMap<String, String>> orderlist2;
    private ArrayList<HashMap<String, String>> orderlist3;
    private ArrayList<HashMap<String, String>> orderlist4;

    private Boolean is_logged_in = false;

    private TextView placeName;
    private TextView signUPBnr;
    private NestedScrollView scrollView;

    private ImageView bnr1, bnr2;
    private String user_id = "";

    private EditText searchItem;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getBoolean("is_logged_in", false);
        user_id = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("user_id", "");
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view1 = findViewById(R.id.recycler_view1);
        recycler_view2 = findViewById(R.id.recycler_view2);
        recycler_view3 = findViewById(R.id.recycler_view3);
        recycler_view4 = findViewById(R.id.recycler_view4);
        bnr1 = findViewById(R.id.bnr1);
        bnr2 = findViewById(R.id.bnr2);
        signUPBnr = findViewById(R.id.signUPBnr);
        placeName = findViewById(R.id.placeName);
        searchItem = findViewById(R.id.searchItem);
        scrollView = findViewById(R.id.scrollView);
        bottumLaySetColor("home");
        if (is_logged_in) {
            signUPBnr.setVisibility(View.GONE);
        } else {
            signUPBnr.setVisibility(View.VISIBLE);
            signUPBnr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent I = new Intent(MainActivity.this, SignInActivity.class);
                    startActivity(I);
                }
            });
        }


        MethodClass.searchKey(MainActivity.this, searchItem);

        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(MainActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(MainActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(MainActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getList();
        MethodClass.placeName(MainActivity.this, placeName);
    }

    public void profile(View view) {
        if (is_logged_in) {
            Intent I = new Intent(MainActivity.this, BuyerDashboardActivity.class);
            startActivity(I);
        } else {
            Intent I = new Intent(MainActivity.this, SignInActivity.class);
            startActivity(I);
        }
    }

    public void getList() {

        if (MethodClass.isNetworkConnected(MainActivity.this)) {
            MethodClass.showProgressDialog(MainActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "home";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("lat", String.valueOf(USER_LAT));
            params.put("lng", String.valueOf(USER_LANG));
            if (is_logged_in) {
                params.put("user_id", user_id);
            } else {
                params.put("user_id", "");
            }
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(MainActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(MainActivity.this, response);
                        if (result_Object != null) {
                            orderlist = new ArrayList<HashMap<String, String>>();
                            orderlist1 = new ArrayList<HashMap<String, String>>();
                            orderlist2 = new ArrayList<HashMap<String, String>>();
                            orderlist3 = new ArrayList<HashMap<String, String>>();
                            orderlist4 = new ArrayList<HashMap<String, String>>();
                            JSONArray category = result_Object.getJSONArray("category");
                            if (category.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(category.length()));
                                for (int i = 0; i < category.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    String name = category.getJSONObject(i).getString("name");
                                    String id = category.getJSONObject(i).getString("id");
                                    String image = category.getJSONObject(i).getString("image");

                                    map.put(CAT_ID, id);
                                    map.put(CAT_NAME, name);
                                    map.put(CAT_IMG, image);
                                    orderlist.add(map);

                                }

                                Home1Adapter adapter = new Home1Adapter(MainActivity.this, orderlist);
                                recycler_view.setAdapter(adapter);
                            }
                            JSONArray homeBanner = result_Object.getJSONArray("homeBanner");
                            if (homeBanner.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(homeBanner.length()));
                                for (int i = 0; i < homeBanner.length(); i++) {


                                    if (i == 0) {
                                        String id = homeBanner.getJSONObject(i).getString("id");
                                        String image = homeBanner.getJSONObject(i).getString("image");

                                        Picasso.get().load(BAN_IMG_URL + image).placeholder(R.drawable.mhts1ele6mbchv0c1).error(R.drawable.mhts1ele6mbchv0c1).into(bnr1);
                                    }
                                    if (i == 1) {
                                        String id = homeBanner.getJSONObject(i).getString("id");
                                        String image = homeBanner.getJSONObject(i).getString("image");

                                        Picasso.get().load(BAN_IMG_URL + image).placeholder(R.drawable.add_image2).error(R.drawable.add_image2).into(bnr2);
                                    }
                                }
                            }

                            JSONArray slider = result_Object.getJSONArray("slider");
                            if (slider.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(slider.length()));
                                for (int i = 0; i < slider.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    String id = slider.getJSONObject(i).getString("id");
                                    String image = slider.getJSONObject(i).getString("name");
                                    String heading_1 = slider.getJSONObject(i).getString("heading_1");
                                    String heading_2 = slider.getJSONObject(i).getString("heading_2");
                                    String heading_3 = slider.getJSONObject(i).getString("heading_3");
                                    String url = slider.getJSONObject(i).getString("url");

                                    map.put(SLIDER_ID, id);
                                    map.put(SLIDER_IMG, image);
                                    map.put(SLIDER_HEAD1, heading_1);
                                    map.put(SLIDER_HEAD2, heading_2);

                                    map.put(SLIDER_HEAD3, heading_3);
                                    map.put(SLIDER_URL, url);
                                    orderlist1.add(map);

                                }

                                Home2Adapter adapter1 = new Home2Adapter(MainActivity.this, orderlist1);
                                recycler_view1.setAdapter(adapter1);
                            }

                            JSONArray hot_selling_product = result_Object.getJSONArray("hot_selling_product");
                            if (hot_selling_product.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(hot_selling_product.length()));
                                for (int i = 0; i < hot_selling_product.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    String id = hot_selling_product.getJSONObject(i).getString("id");
                                    String name = hot_selling_product.getJSONObject(i).getString("name");
                                    String description = hot_selling_product.getJSONObject(i).getString("description");
                                    String starting_price = hot_selling_product.getJSONObject(i).getString("starting_price");
                                    String starting_disc_price = hot_selling_product.getJSONObject(i).getString("starting_disc_price");
                                    String is_fav = hot_selling_product.getJSONObject(i).getString("is_fav");
                                    String img = hot_selling_product.getJSONObject(i).getString("img");

                                    JSONObject imgObj = new JSONObject(img);

                                    map.put(PRO_ID, id);
                                    map.put(PRO_NAME, name);
                                    map.put(PRO_DESC, description);
                                    map.put(PRO_PRICE, starting_price);
                                    map.put(IS_FAV, is_fav);

                                    map.put(PRO_DISC, starting_disc_price);
                                    map.put(PRO_IMG, imgObj.getString("image"));
                                    orderlist2.add(map);

                                }

                                Home3Adapter adapter2 = new Home3Adapter(MainActivity.this, orderlist2);
                                recycler_view2.setAdapter(adapter2);
                            }

                            JSONArray featured_product = result_Object.getJSONArray("featured_product");
                            if (featured_product.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(featured_product.length()));
                                for (int i = 0; i < featured_product.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    String id = featured_product.getJSONObject(i).getString("id");
                                    String name = featured_product.getJSONObject(i).getString("name");
                                    String description = featured_product.getJSONObject(i).getString("description");
                                    String starting_price = featured_product.getJSONObject(i).getString("starting_price");
                                    String starting_disc_price = featured_product.getJSONObject(i).getString("starting_disc_price");
                                    String is_fav = featured_product.getJSONObject(i).getString("is_fav");
                                    String img = featured_product.getJSONObject(i).getString("img");

                                    JSONObject imgObj = new JSONObject(img);

                                    map.put(PRO_ID, id);
                                    map.put(PRO_NAME, name);
                                    map.put(PRO_DESC, description);
                                    map.put(PRO_PRICE, starting_price);
                                    map.put(IS_FAV, is_fav);

                                    map.put(PRO_DISC, starting_disc_price);
                                    map.put(PRO_IMG, imgObj.getString("image"));
                                    orderlist4.add(map);
                                }
                                Home5Adapter adapter4 = new Home5Adapter(MainActivity.this, orderlist4);
                                recycler_view4.setAdapter(adapter4);
                            }
                            JSONArray company = result_Object.getJSONArray("company");
                            if (company.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(company.length()));
                                for (int i = 0; i < company.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    String id = company.getJSONObject(i).getString("id");
                                    String company_name = company.getJSONObject(i).getString("company_name");
                                    String full_address = company.getJSONObject(i).getString("full_address");
                                    String avg_review = company.getJSONObject(i).getString("avg_review");
                                    String profile_pic = company.getJSONObject(i).getString("profile_pic");

                                    map.put(COM_ID, id);
                                    map.put(COM_NAME, company_name);
                                    map.put(COM_IMG, profile_pic);
                                    map.put(COM_RATE, avg_review);
                                    map.put(COM_ADD, full_address);


                                    orderlist3.add(map);
                                }
                                Home4Adapter adapter3 = new Home4Adapter(MainActivity.this, orderlist3);
                                recycler_view3.setAdapter(adapter3);
                            }

                            scrollView.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(MainActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(MainActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(MainActivity.this);
                    } else {
                        MethodClass.error_alert(MainActivity.this);
                    }

                }
            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if (!PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("token", "").equals("")) {
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }

    public void browse(View view) {
        Intent I = new Intent(this, SearchResultActivity.class);
        startActivity(I);
    }

    public void home(View view) {

    }

    public void shoppingcart(View view) {
        Intent I = new Intent(this, CartActivity.class);
        startActivity(I);
    }

    public void btmMore(View view) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(MainActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if (is_logged_in) {
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        } else {
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Address Book")) {
                    if (is_logged_in) {
                        Intent I = new Intent(MainActivity.this, BuyerAddressBookActivity.class);
                        startActivity(I);
                    } else {
                        Intent I = new Intent(MainActivity.this, SignInActivity.class);
                        startActivity(I);
                    }

                }
                if (item.getTitle().equals("Edit Profile")) {
                    if (is_logged_in) {
                        Intent I = new Intent(MainActivity.this, EditProfileActivity.class);
                        startActivity(I);
                    } else {
                        Intent I = new Intent(MainActivity.this, SignInActivity.class);
                        startActivity(I);
                    }

                }
                if (item.getTitle().equals("Dashboard")) {
                    if (is_logged_in) {
                        Intent I = new Intent(MainActivity.this, BuyerDashboardActivity.class);
                        startActivity(I);
                    } else {
                        Intent I = new Intent(MainActivity.this, SignInActivity.class);
                        startActivity(I);
                    }

                }
                if (item.getTitle().equals("Favourites")) {
                    if (is_logged_in) {
                        Intent I = new Intent(MainActivity.this, FavouritesActivity.class);
                        startActivity(I);
                    } else {
                        Intent I = new Intent(MainActivity.this, SignInActivity.class);
                        startActivity(I);
                    }

                }
                if (item.getTitle().equals("Orders")) {
                    if (is_logged_in) {
                        Intent I = new Intent(MainActivity.this, OrderListActivity.class);
                        startActivity(I);
                    } else {
                        Intent I = new Intent(MainActivity.this, SignInActivity.class);
                        startActivity(I);
                    }

                }
                if (item.getTitle().equals("Logout")) {
                    MethodClass.log_out(MainActivity.this);

                }
                if (item.getTitle().equals("Sign In")) {
                    Intent I = new Intent(MainActivity.this, SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    private void bottumLaySetColor(String s) {
        TextView home_tv, browse_tv, cart_tv, login_tv;
        ImageView home_img, browse_img, cart_img, login_img;

        LinearLayout login_layout, cart_layout, browse_layout, home_layout;

        home_tv = (TextView) findViewById(R.id.home_tv);
        browse_tv = (TextView) findViewById(R.id.browse_tv);
        cart_tv = (TextView) findViewById(R.id.cart_tv);
        login_tv = (TextView) findViewById(R.id.login_tv);

        home_img = (ImageView) findViewById(R.id.home_img);
        browse_img = (ImageView) findViewById(R.id.browse_img);
        cart_img = (ImageView) findViewById(R.id.cart_img);
        login_img = (ImageView) findViewById(R.id.login_img);


        login_layout = (LinearLayout) findViewById(R.id.login_layout);
        cart_layout = (LinearLayout) findViewById(R.id.cart_layout);
        browse_layout = (LinearLayout) findViewById(R.id.browse_layout);
        home_layout = (LinearLayout) findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")) {
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")) {
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")) {
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")) {
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }
}
