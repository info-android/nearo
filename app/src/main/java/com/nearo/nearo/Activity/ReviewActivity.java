package com.nearo.nearo.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.OrderDetailAdapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.CART_ID;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.PRO_RECIE;
import static com.nearo.nearo.Helper.Constants.PRO_REV;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class ReviewActivity extends AppCompatActivity {
    private Boolean is_logged_in = false;
    private TextView placeName;
    private TextView pro_name;
    private TextView pro_number;
    private TextView pro_date;
    private EditText searchItem;
    private String user_id;

    private Button save;
    private ScaleRatingBar comRatingBar;
    private EditText rev_head;
    private EditText gAdd;
    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        placeName = findViewById(R.id.placeName);
        searchItem  = findViewById(R.id.searchItem);
        pro_number  = findViewById(R.id.pro_number);
        pro_date  = findViewById(R.id.pro_date);
        pro_name  = findViewById(R.id.pro_name);
        save  = findViewById(R.id.save);

        comRatingBar  = findViewById(R.id.comRatingBar);
        rev_head  = findViewById(R.id.rev_head);
        gAdd  = findViewById(R.id.gAdd);

        user_id = PreferenceManager.getDefaultSharedPreferences(ReviewActivity.this).getString("user_id","");
        bottumLaySetColor("more");
        MethodClass.placeName(ReviewActivity.this,placeName);
        MethodClass.searchKey(ReviewActivity.this,searchItem);

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(ReviewActivity.this).getBoolean("is_logged_in",false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRate();
            }
        });
        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(ReviewActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(ReviewActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(ReviewActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(ReviewActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        getProduct();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }
    public void saveRate(){
        if (MethodClass.isNetworkConnected(ReviewActivity.this)) {
            if(comRatingBar.getRating() == 0){
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please give rating first", Snackbar.LENGTH_SHORT);
                snackbar.show();
                return;
            }
            if(rev_head.getText().toString().trim().length() == 0){
                rev_head.setError("Please give review heading");
                rev_head.requestFocus();
                return;
            }
            if(gAdd.getText().toString().trim().length() == 0){
                gAdd.setError("Please give review");
                gAdd.requestFocus();
                return;
            }
            MethodClass.showProgressDialog(ReviewActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "product-review-store";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("order_details_id",getIntent().getStringExtra("ord_id"));
            params.put("product_id",getIntent().getStringExtra("pro_id"));
            params.put("review_point", String.valueOf(comRatingBar.getRating()));
            params.put("review_heading", rev_head.getText().toString().trim());
            params.put("review_comment", gAdd.getText().toString().trim());
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(ReviewActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(ReviewActivity.this, response);
                        if (result_Object != null) {
                            String message = result_Object.getString("message");
                            new SweetAlertDialog(ReviewActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Successful")
                                    .setContentText("Review posted successfully")
                                    .setConfirmText("Ok")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(ReviewActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(ReviewActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(ReviewActivity.this);
                    } else {
                        MethodClass.error_alert(ReviewActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(ReviewActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ReviewActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void getProduct(){
        if (MethodClass.isNetworkConnected(ReviewActivity.this)) {
            MethodClass.showProgressDialog(ReviewActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "post-review";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("order_details_id",getIntent().getStringExtra("ord_id"));
            params.put("product_id",getIntent().getStringExtra("pro_id"));
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(ReviewActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(ReviewActivity.this, response);
                        if (result_Object != null) {
                            String message = result_Object.getString("message");
                            pro_name.setText(result_Object.getString("product_name"));
                            pro_number.setText(result_Object.getString("order_number"));
                            pro_date.setText(result_Object.getString("order_date"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(ReviewActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(ReviewActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(ReviewActivity.this);
                    } else {
                        MethodClass.error_alert(ReviewActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(ReviewActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ReviewActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }


    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(ReviewActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                        Intent I  = new Intent(ReviewActivity.this,BuyerAddressBookActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ReviewActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){
                        Intent I  = new Intent(ReviewActivity.this,EditProfileActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ReviewActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(ReviewActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ReviewActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(ReviewActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ReviewActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(ReviewActivity.this,OrderListActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ReviewActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(ReviewActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(ReviewActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(ReviewActivity.this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(ReviewActivity.this,SignInActivity.class);
            startActivity(I);
        }
    }
    private void bottumLaySetColor(String s) {
        TextView home_tv, browse_tv, cart_tv, login_tv;
        ImageView home_img, browse_img, cart_img, login_img;

        LinearLayout login_layout, cart_layout, browse_layout, home_layout;

        home_tv = (TextView) findViewById(R.id.home_tv);
        browse_tv = (TextView) findViewById(R.id.browse_tv);
        cart_tv = (TextView) findViewById(R.id.cart_tv);
        login_tv = (TextView) findViewById(R.id.login_tv);

        home_img = (ImageView) findViewById(R.id.home_img);
        browse_img = (ImageView) findViewById(R.id.browse_img);
        cart_img = (ImageView) findViewById(R.id.cart_img);
        login_img = (ImageView) findViewById(R.id.login_img);


        login_layout = (LinearLayout) findViewById(R.id.login_layout);
        cart_layout = (LinearLayout) findViewById(R.id.cart_layout);
        browse_layout = (LinearLayout) findViewById(R.id.browse_layout);
        home_layout = (LinearLayout) findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")) {
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")) {
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")) {
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")) {
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }
}
