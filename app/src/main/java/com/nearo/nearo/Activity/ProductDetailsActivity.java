package com.nearo.nearo.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.CartAdapter;
import com.nearo.nearo.Adapter.Home1Adapter;
import com.nearo.nearo.Adapter.Home2Adapter;
import com.nearo.nearo.Adapter.Home3Adapter;
import com.nearo.nearo.Adapter.ReviewAdapter;
import com.nearo.nearo.Adapter.SellerProductAdapter;
import com.nearo.nearo.Adapter.cartProAdapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.COM_ID;
import static com.nearo.nearo.Helper.Constants.COM_IMG_URL;
import static com.nearo.nearo.Helper.Constants.DEVICE_ID;
import static com.nearo.nearo.Helper.Constants.FNAME;
import static com.nearo.nearo.Helper.Constants.IS_FAV;
import static com.nearo.nearo.Helper.Constants.LNAME;
import static com.nearo.nearo.Helper.Constants.PICK_LANG;
import static com.nearo.nearo.Helper.Constants.PICK_LAT;
import static com.nearo.nearo.Helper.Constants.PRODUCT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_DISC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.REV_COMM;
import static com.nearo.nearo.Helper.Constants.REV_DATE;
import static com.nearo.nearo.Helper.Constants.REV_HEAD;
import static com.nearo.nearo.Helper.Constants.REV_POINT;
import static com.nearo.nearo.Helper.Constants.SELLERS;
import static com.nearo.nearo.Helper.Constants.SUB_NAME;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class ProductDetailsActivity extends AppCompatActivity {
    private RecyclerView recycler_view2;
    private ArrayList<HashMap<String, String>> orderlist=new ArrayList<HashMap<String, String>>();
    private Boolean is_logged_in = false;
    private TextView placeName;
    private String user_id = "";

    private CarouselView carouselView;
    String[] mStrings;

    private TextView pro_name,pro_price,pro_Cat,offPer,proAbt,comName,comLoc,comLoc2,quan,gAdd;
    private Spinner price_spinner;
    private ScaleRatingBar comRatingBar;
    private ImageView comImg;
    private ImageView fav;
    private ImageView plus;
    private ImageView minus;
    private Button viewSeller;
    private EditText searchItem;
    private NestedScrollView scrollView;
    int quann = 1;
    String price_id;
    private TextView addToCart;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        recycler_view2  = findViewById(R.id.recycler_view2);
        placeName = findViewById(R.id.placeName);
        searchItem  = findViewById(R.id.searchItem);
        scrollView  = findViewById(R.id.scrollView);
        plus  = findViewById(R.id.plus);
        minus  = findViewById(R.id.minus);
        quan  = findViewById(R.id.quan);

        user_id = PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("user_id","");
        bottumLaySetColor("mores");
        MethodClass.placeName(ProductDetailsActivity.this,placeName);
        MethodClass.searchKey(ProductDetailsActivity.this,searchItem);

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in",false);
        carouselView =  findViewById(R.id.carouselView);

        pro_name =  findViewById(R.id.pro_name);
        pro_Cat =  findViewById(R.id.pro_Cat);
        offPer =  findViewById(R.id.offPer);
        proAbt =  findViewById(R.id.proAbt);
        comName =  findViewById(R.id.comName);
        comLoc =  findViewById(R.id.comLoc);
        comLoc2 =  findViewById(R.id.comLoc2);
        pro_price =  findViewById(R.id.pro_price);
        price_spinner =  findViewById(R.id.price_spinner);
        comRatingBar =  findViewById(R.id.comRatingBar);
        comImg =  findViewById(R.id.comImg);
        addToCart =  findViewById(R.id.addToCart);
        fav =  findViewById(R.id.fav);
        viewSeller =  findViewById(R.id.viewSeller);

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quann = quann+1;
                quan.setText(String.valueOf(quann));
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quann>1){
                    quann = quann-1;
                    quan.setText(String.valueOf(quann));
                }
            }
        });
        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(ProductDetailsActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(ProductDetailsActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(ProductDetailsActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(ProductDetailsActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        getList();




    }

    public void getList(){
        if (MethodClass.isNetworkConnected(ProductDetailsActivity.this)) {
            MethodClass.showProgressDialog(ProductDetailsActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "product-details";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("product_id", getIntent().getStringExtra("pro_id"));
            if(is_logged_in){
                params.put("user_id", user_id);
            }else {
                params.put("user_id", "");
            }

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                        if (result_Object != null) {

                            String message  = result_Object.getString("message");

                            JSONObject productsArr = result_Object.getJSONObject("product");

                            String name = productsArr.getString("name");
                            String description = productsArr.getString("description");
                            String category = productsArr.getString("category");
                            String sub_category = productsArr.getString("sub_category");
                            String seller_name = productsArr.getString("seller_name");
                            String is_fav = productsArr.getString("is_fav");

                            if(is_fav.equals("Y")){
                                fav.setImageDrawable(getResources().getDrawable(R.drawable.home_9));
                            }else{
                                fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_heart));
                            }
                            fav.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(is_logged_in){
                                        if(fav.equals("Y")){
                                            MethodClass.showProgressDialog(ProductDetailsActivity.this);
                                            String server_url = ProductDetailsActivity.this.getString(R.string.SERVER_URL) + "add-to-favorite";
                                            HashMap<String, String> params = new HashMap<String, String>();
                                            params.put("product_id", getIntent().getStringExtra("pro_id"));
                                            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    Log.e("resp", response.toString());
                                                    try{
                                                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                        JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                                                        if (result_Object != null) {
                                                            String message = result_Object.getString("message");
                                                            fav.setImageDrawable(ProductDetailsActivity.this.getDrawable(R.drawable.ic_heart));
                                                            new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                                    .setTitleText("Removed!")
                                                                    .setContentText("Product removed from your favourite list")
                                                                    .setConfirmText("Ok")
                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sDialog) {
                                                                            sDialog.dismissWithAnimation();
                                                                        }
                                                                    })
                                                                    .show();
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                        Log.e("error", e.getMessage());
                                                    }


                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {

                                                    MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                    Log.e("error", error.toString());
                                                    if (error.toString().contains("AuthFailureError")) {
                                                        Toast.makeText(ProductDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(ProductDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }) {
                                                //* Passing some request headers*
                                                @Override
                                                public Map getHeaders() throws AuthFailureError {
                                                    HashMap headers = new HashMap();
                                                    headers.put("Content-Type", "application/json");
                                                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));

                                                    Log.e("getHeaders: ", headers.toString());

                                                    return headers;
                                                }
                                            };

                                            MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
                                        }else{
                                            MethodClass.showProgressDialog(ProductDetailsActivity.this);
                                            String server_url = ProductDetailsActivity.this.getString(R.string.SERVER_URL) + "add-to-favorite";
                                            HashMap<String, String> params = new HashMap<String, String>();
                                            params.put("product_id", getIntent().getStringExtra("pro_id"));
                                            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    Log.e("resp", response.toString());
                                                    try{
                                                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                        JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                                                        if (result_Object != null) {
                                                            String message = result_Object.getString("message");
                                                            fav.setImageDrawable(ProductDetailsActivity.this.getDrawable(R.drawable.home_9));
                                                            new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                                    .setTitleText("Added!")
                                                                    .setContentText("Product added to your favourite list")
                                                                    .setConfirmText("Ok")
                                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                        @Override
                                                                        public void onClick(SweetAlertDialog sDialog) {
                                                                            sDialog.dismissWithAnimation();
                                                                        }
                                                                    })
                                                                    .show();
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                        Log.e("error", e.getMessage());
                                                    }


                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {

                                                    MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                    Log.e("error", error.toString());
                                                    if (error.toString().contains("AuthFailureError")) {
                                                        Toast.makeText(ProductDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(ProductDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }) {
                                                //* Passing some request headers*
                                                @Override
                                                public Map getHeaders() throws AuthFailureError {
                                                    HashMap headers = new HashMap();
                                                    headers.put("Content-Type", "application/json");
                                                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));

                                                    Log.e("getHeaders: ", headers.toString());

                                                    return headers;
                                                }
                                            };

                                            MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
                                        }

                                    }else {
                                        new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                .setTitleText("Login Required!")
                                                .setContentText("Please login to add this item as favourite")
                                                .setConfirmText("Login")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                        Intent I= new Intent(ProductDetailsActivity.this, SignInActivity.class);
                                                        startActivity(I);
                                                    }
                                                })
                                                .show();
                                    }
                                }
                            });
                            JSONObject seller_nameObj = new JSONObject(seller_name);
                            String company_name = seller_nameObj.getString("company_name");
                            String full_address = seller_nameObj.getString("full_address");
                            String zip_code = seller_nameObj.getString("zip_code");
                            String avg_review = seller_nameObj.getString("avg_review");
                            String profile_pic = seller_nameObj.getString("profile_pic");
                            final String com_id = seller_nameObj.getString("id");
                            viewSeller.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent I = new Intent(ProductDetailsActivity.this, SellerPublicProfileActivity.class);
                                    I.putExtra("com_id",com_id);
                                    startActivity(I);
                                }
                            });
                            comName.setText(company_name);
                            comLoc.setText(full_address);
                            comLoc2.setText(zip_code);
                            comRatingBar.setRating(Float.parseFloat(avg_review));
                            Picasso.get().load(COM_IMG_URL+profile_pic).placeholder(R.drawable.user1).error(R.drawable.user1).into(comImg);


                            JSONObject catObj = new JSONObject(category);
                            String catName = catObj.getString("name");

                            JSONObject subObj = new JSONObject(sub_category);
                            String subName = subObj.getString("name");

                            pro_name.setText(name);
                            pro_Cat.setText(catName+","+subName);
                            proAbt.setText(Html.fromHtml(description));
                            String unit_price = productsArr.getString("unit_price");

                            JSONArray unitArr = new JSONArray(unit_price);
                            List<StringWithTag> spinnerArrays = new ArrayList<StringWithTag>();
                            for (int i = 0; i <unitArr.length() ; i++) {
                                String id = unitArr.getJSONObject(i).getString("id");
                                String quantity = unitArr.getJSONObject(i).getString("quantity");
                                String price = unitArr.getJSONObject(i).getString("price");
                                String discount_price = unitArr.getJSONObject(i).getString("discount_price");
                                String unit_name = unitArr.getJSONObject(i).getString("unit_name");



                                JSONObject unitName = new JSONObject(unit_name);
                                String unitN = unitName.getString("name");
                                if(Double.parseDouble(price) == Double.parseDouble(discount_price)){
                                    spinnerArrays.add(new StringWithTag(quantity+unitN+"  ₹"+price,price,discount_price, id));
                                }else{
                                    spinnerArrays.add(new StringWithTag(quantity+unitN+"  ₹"+discount_price,price,discount_price, id));
                                }
                            }
                            ArrayAdapter adapter3 = new ArrayAdapter(ProductDetailsActivity.this, android.R.layout.simple_spinner_dropdown_item, spinnerArrays) {
                            };
                            price_spinner.setAdapter(adapter3);
                            price_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);

                                    String prices = s.string2;
                                    String disc_prices = s.string3;

                                    if(Double.parseDouble(prices) == Double.parseDouble(disc_prices)){
                                        offPer.setVisibility(View.GONE);
                                        pro_price.setText("₹"+prices);
                                    }else{
                                        offPer.setText(String.format("%.2f",((Double.parseDouble(prices)-Double.parseDouble(disc_prices))/Double.parseDouble(prices))*100)+"% off");
                                        offPer.setVisibility(View.VISIBLE);
                                        pro_price.setText("₹"+disc_prices);
                                    }
                                    price_id = String.valueOf(s.tag);
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });

                            JSONArray imgArr = productsArr.getJSONArray("imgs");


                            mStrings = new String[imgArr.length()];
                            for (int m = 0; m <imgArr.length() ; m++) {
                                mStrings[m] = imgArr.getJSONObject(m).getString("image");
                            }
                            carouselView.setImageListener(imageListener);
                            carouselView.setPageCount(mStrings.length);


                            JSONArray hot_product = result_Object.getJSONArray("hot_product");
                            if (hot_product.length() != 0) {

                                Log.e("featured_jsonArray", String.valueOf(hot_product.length()));
                                for (int i = 0; i < hot_product.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    JSONObject productsObj = hot_product.getJSONObject(i);
                                    String id = productsObj.getString("id");
                                    String names = productsObj.getString("name");
                                    String descriptions = productsObj.getString("description");
                                    String starting_price =productsObj.getString("starting_price");
                                    String starting_disc_price =productsObj.getString("starting_disc_price");

                                    String img = productsObj.getString("img");

                                    JSONObject imgObj = new JSONObject(img);

                                    map.put(PRO_ID,id);
                                    map.put(PRO_NAME,names);
                                    map.put(PRO_DESC,descriptions);
                                    map.put(PRO_PRICE,starting_price);
                                    map.put(PRO_DISC,starting_disc_price);
                                    map.put(PRO_IMG,imgObj.getString("image"));
                                    orderlist.add(map);

                                }
                                SellerProductAdapter adapter = new SellerProductAdapter(ProductDetailsActivity.this, orderlist);
                                recycler_view2.setAdapter(adapter);
                            }


                            scrollView.setVisibility(View.VISIBLE);

                            addToCart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (MethodClass.isNetworkConnected(ProductDetailsActivity.this)) {
                                        MethodClass.showProgressDialog(ProductDetailsActivity.this);
                                        String server_url = getString(R.string.SERVER_URL) + "product-details";
                                        HashMap<String, String> params = new HashMap<String, String>();
                                        params.put("product_id", getIntent().getStringExtra("pro_id"));
                                        if(is_logged_in){

                                        }else {
                                            params.put("divice_id", DEVICE_ID);
                                        }

                                        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                                            @Override
                                            public void onResponse(JSONObject response) {
                                                Log.e("HomeRes", response.toString());

                                                try {
                                                    MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                    JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                                                    if (result_Object != null) {

                                                        String message  = result_Object.getString("message");

                                                        JSONObject productsArr = result_Object.getJSONObject("product");

                                                        String name = productsArr.getString("name");
                                                        String description = productsArr.getString("description");
                                                        String category = productsArr.getString("category");
                                                        String sub_category = productsArr.getString("sub_category");
                                                        String seller_name = productsArr.getString("seller_name");
                                                        JSONObject seller_nameObj = new JSONObject(seller_name);
                                                        final String com_id = seller_nameObj.getString("id");

                                                        JSONObject catObj = new JSONObject(category);
                                                        String catName = catObj.getString("name");

                                                        JSONObject subObj = new JSONObject(sub_category);
                                                        String subName = subObj.getString("name");
                                                        String unit_price = productsArr.getString("unit_price");

                                                        final Dialog dialog = new Dialog(ProductDetailsActivity.this);
                                                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                                        dialog.setContentView(R.layout.add_to_cart2);
                                                        ImageView close = (ImageView) dialog.findViewById(R.id.close);
                                                        gAdd = (TextView) dialog.findViewById(R.id.gAdd);
                                                        final RecyclerView recycler_view4 = (RecyclerView) dialog.findViewById(R.id.recycler_view4);
                                                        TextView addToCartss = (TextView) dialog.findViewById(R.id.addToCartss);
                                                        Button check = (Button) dialog.findViewById(R.id.check);
                                                        final TextView txt = (TextView) dialog.findViewById(R.id.txt);

                                                        MethodClass.placeName(ProductDetailsActivity.this,gAdd);
                                                        // Initialize Places.
                                                        Places.initialize(ProductDetailsActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                                                        // Create a new Places client instance.
                                                        PlacesClient placesClient = Places.createClient(ProductDetailsActivity.this);
                                                        // Set the fields to specify which types of place data to return.
                                                        final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                                                        gAdd.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                Intent intent = new Autocomplete.IntentBuilder(
                                                                        AutocompleteActivityMode.OVERLAY, fields)
                                                                        .build(ProductDetailsActivity.this);
                                                                ProductDetailsActivity.this.startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                                                                new Handler().postDelayed(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        MethodClass.placeName(ProductDetailsActivity.this,gAdd);
                                                                    }
                                                                }, 8000);
                                                            }
                                                        });

                                                        close.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                dialog.dismiss();
                                                            }
                                                        });
                                                        dialog.show();

                                                        check.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                MethodClass.showProgressDialog(ProductDetailsActivity.this);
                                                                String server_url = getString(R.string.SERVER_URL) + "check-location";
                                                                HashMap<String, String> params = new HashMap<String, String>();
                                                                params.put("location", gAdd.getText().toString());
                                                                params.put("lat",String.valueOf(USER_LAT));
                                                                params.put("lng", String.valueOf(USER_LANG));
                                                                params.put("seller_id",com_id);
                                                                if(is_logged_in){
                                                                    params.put("user_id",user_id);
                                                                }else {
                                                                    params.put("divice_id",DEVICE_ID);
                                                                }
                                                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                                    @Override
                                                                    public void onResponse(JSONObject response) {
                                                                        Log.e("resp", response.toString());
                                                                        try{
                                                                            MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                            JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                                                                            if (result_Object != null) {
                                                                                String message = result_Object.getString("message");
                                                                                txt.setText(message);

                                                                                JSONArray proArr = result_Object.getJSONArray("product");
                                                                                if(proArr.length()>0){
                                                                                    orderlist = new ArrayList<HashMap<String, String>>();
                                                                                    SELLERS = new String[proArr.length()];
                                                                                    for (int i = 0; i <proArr.length() ; i++) {
                                                                                        HashMap<String,String> mapa = new HashMap<String, String>();
                                                                                        JSONObject pObj = proArr.getJSONObject(i);
                                                                                        String id = pObj.getString("id");
                                                                                        SELLERS[i] = id;

                                                                                        String name = pObj.getString("name");

                                                                                        mapa.put(SUB_NAME,name);
                                                                                        orderlist.add(mapa);

                                                                                    }

                                                                                    cartProAdapter adapter1 = new cartProAdapter(ProductDetailsActivity.this, orderlist);
                                                                                    recycler_view4.setAdapter(adapter1);
                                                                                }
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                            MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                                            Log.e("error", e.getMessage());
                                                                        }


                                                                    }
                                                                }, new Response.ErrorListener() {
                                                                    @Override
                                                                    public void onErrorResponse(VolleyError error) {

                                                                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                        Log.e("error", error.toString());
                                                                        if (error.toString().contains("AuthFailureError")) {
                                                                            Toast.makeText(ProductDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                                        } else {
                                                                            Toast.makeText(ProductDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                }) {
                                                                    //* Passing some request headers*
                                                                    @Override
                                                                    public Map getHeaders() throws AuthFailureError {
                                                                        HashMap headers = new HashMap();
                                                                        headers.put("Content-Type", "application/json");
                                                                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));

                                                                        Log.e("getHeaders: ", headers.toString());

                                                                        return headers;
                                                                    }
                                                                };

                                                                MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);

                                                            }
                                                        });
                                                        addToCartss.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                MethodClass.showProgressDialog(ProductDetailsActivity.this);
                                                                String server_url = getString(R.string.SERVER_URL) + "check-location";
                                                                HashMap<String, String> params = new HashMap<String, String>();
                                                                params.put("location", gAdd.getText().toString());
                                                                params.put("lat",String.valueOf(USER_LAT));
                                                                params.put("lng", String.valueOf(USER_LANG));
                                                                params.put("seller_id",com_id);
                                                                if(is_logged_in){
                                                                    params.put("user_id",user_id);
                                                                }else {
                                                                    params.put("divice_id",DEVICE_ID);
                                                                }
                                                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                                    @Override
                                                                    public void onResponse(JSONObject response) {
                                                                        Log.e("resp", response.toString());
                                                                        try{
                                                                            MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                            JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                                                                            if (result_Object != null) {
                                                                                String message = result_Object.getString("message");
                                                                                txt.setText(message);

                                                                                JSONArray proArr = result_Object.getJSONArray("product");
                                                                                if(proArr.length()>0){
                                                                                    orderlist = new ArrayList<HashMap<String, String>>();
                                                                                    SELLERS = new String[proArr.length()];
                                                                                    for (int i = 0; i <proArr.length() ; i++) {
                                                                                        HashMap<String,String> mapa = new HashMap<String, String>();
                                                                                        JSONObject pObj = proArr.getJSONObject(i);
                                                                                        String id = pObj.getString("id");
                                                                                        SELLERS[i] = id;

                                                                                        String name = pObj.getString("name");

                                                                                        mapa.put(SUB_NAME,name);
                                                                                        orderlist.add(mapa);

                                                                                    }

                                                                                    cartProAdapter adapter1 = new cartProAdapter(ProductDetailsActivity.this, orderlist);
                                                                                    recycler_view4.setAdapter(adapter1);
                                                                                }

                                                                                if(proArr.length()>0){
                                                                                    new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.WARNING_TYPE)
                                                                                            .setTitleText("Delete Product")
                                                                                            .setContentText("If you add this product then the listed cart items will be deleted. Do you want to add this?")
                                                                                            .setConfirmText("Yes")
                                                                                            .setCancelText("No")
                                                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                                @Override
                                                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                                                    sDialog.dismissWithAnimation();
                                                                                                    MethodClass.showProgressDialog(ProductDetailsActivity.this);
                                                                                                    String server_url = ProductDetailsActivity.this.getString(R.string.SERVER_URL) + "add-to-cart";
                                                                                                    HashMap<String, String> params = new HashMap<String, String>();
                                                                                                    params.put("location", gAdd.getText().toString());
                                                                                                    params.put("lat",String.valueOf(USER_LAT));
                                                                                                    params.put("lng", String.valueOf(USER_LANG));
                                                                                                    params.put("seller_id",com_id);
                                                                                                    params.put("product_id",getIntent().getStringExtra("pro_id"));
                                                                                                    params.put("price_id",price_id);
                                                                                                    params.put("delete_product_id", TextUtils.join(",",SELLERS));
                                                                                                    params.put("qty",String.valueOf(quann));
                                                                                                    if(is_logged_in){
                                                                                                        params.put("user_id",user_id);
                                                                                                    }else {
                                                                                                        params.put("divice_id",DEVICE_ID);
                                                                                                    }
                                                                                                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                                                                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                                                                        @Override
                                                                                                        public void onResponse(JSONObject response) {
                                                                                                            Log.e("resp", response.toString());
                                                                                                            try{
                                                                                                                MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                                                                JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                                                                                                                if (result_Object != null) {
                                                                                                                    String message = result_Object.getString("message");
                                                                                                                    dialog.dismiss();
                                                                                                                    new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                                                                                            .setTitleText("Added")
                                                                                                                            .setContentText("Product added to cart")
                                                                                                                            .setConfirmText("Okay")
                                                                                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                                                                @Override
                                                                                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                                                                                    sDialog.dismissWithAnimation();
                                                                                                                                }
                                                                                                                            })
                                                                                                                            .show();
                                                                                                                }
                                                                                                            } catch (JSONException e) {
                                                                                                                e.printStackTrace();
                                                                                                                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                                                                                Log.e("error", e.getMessage());
                                                                                                            }


                                                                                                        }
                                                                                                    }, new Response.ErrorListener() {
                                                                                                        @Override
                                                                                                        public void onErrorResponse(VolleyError error) {

                                                                                                            MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                                                            Log.e("error", error.toString());
                                                                                                            if (error.toString().contains("AuthFailureError")) {
                                                                                                                Toast.makeText(ProductDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                                                                            } else {
                                                                                                                Toast.makeText(ProductDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                                                                            }
                                                                                                        }
                                                                                                    }) {
                                                                                                        //* Passing some request headers*
                                                                                                        @Override
                                                                                                        public Map getHeaders() throws AuthFailureError {
                                                                                                            HashMap headers = new HashMap();
                                                                                                            headers.put("Content-Type", "application/json");
                                                                                                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));

                                                                                                            Log.e("getHeaders: ", headers.toString());

                                                                                                            return headers;
                                                                                                        }
                                                                                                    };

                                                                                                    MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
                                                                                                }
                                                                                            })
                                                                                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                                @Override
                                                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                                                    sDialog.dismissWithAnimation();
                                                                                                    dialog.dismiss();
                                                                                                }
                                                                                            })
                                                                                            .show();
                                                                                }else {
                                                                                    MethodClass.showProgressDialog(ProductDetailsActivity.this);
                                                                                    String server_url = getString(R.string.SERVER_URL) + "add-to-cart";
                                                                                    HashMap<String, String> params = new HashMap<String, String>();
                                                                                    params.put("location", gAdd.getText().toString());
                                                                                    params.put("lat",String.valueOf(USER_LAT));
                                                                                    params.put("lng", String.valueOf(USER_LANG));
                                                                                    params.put("seller_id",com_id);
                                                                                    params.put("product_id",getIntent().getStringExtra("pro_id"));
                                                                                    params.put("price_id",price_id);
                                                                                    params.put("delete_product_id", "");
                                                                                    params.put("qty",String.valueOf(quann));
                                                                                    if(is_logged_in){
                                                                                        params.put("user_id",user_id);
                                                                                    }else {
                                                                                        params.put("divice_id",DEVICE_ID);
                                                                                    }
                                                                                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                                                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                                                        @Override
                                                                                        public void onResponse(JSONObject response) {
                                                                                            Log.e("resp", response.toString());
                                                                                            try{
                                                                                                MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                                                JSONObject result_Object = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                                                                                                if (result_Object != null) {
                                                                                                    String message = result_Object.getString("message");
                                                                                                    dialog.dismiss();
                                                                                                    new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                                                                                            .setTitleText("Added")
                                                                                                            .setContentText("Product added to cart")
                                                                                                            .setConfirmText("Okay")
                                                                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                                                @Override
                                                                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                                                                    sDialog.dismissWithAnimation();
                                                                                                                }
                                                                                                            })
                                                                                                            .show();
                                                                                                }
                                                                                            } catch (JSONException e) {
                                                                                                e.printStackTrace();
                                                                                                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                                                                Log.e("error", e.getMessage());
                                                                                            }


                                                                                        }
                                                                                    }, new Response.ErrorListener() {
                                                                                        @Override
                                                                                        public void onErrorResponse(VolleyError error) {

                                                                                            MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                                            Log.e("error", error.toString());
                                                                                            if (error.toString().contains("AuthFailureError")) {
                                                                                                Toast.makeText(ProductDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                                                            } else {
                                                                                                Toast.makeText(ProductDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                                                            }
                                                                                        }
                                                                                    }) {
                                                                                        //* Passing some request headers*
                                                                                        @Override
                                                                                        public Map getHeaders() throws AuthFailureError {
                                                                                            HashMap headers = new HashMap();
                                                                                            headers.put("Content-Type", "application/json");
                                                                                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));

                                                                                            Log.e("getHeaders: ", headers.toString());

                                                                                            return headers;
                                                                                        }
                                                                                    };

                                                                                    MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
                                                                                }
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                            MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                                            Log.e("error", e.getMessage());
                                                                        }


                                                                    }
                                                                }, new Response.ErrorListener() {
                                                                    @Override
                                                                    public void onErrorResponse(VolleyError error) {

                                                                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);

                                                                        Log.e("error", error.toString());
                                                                        if (error.toString().contains("AuthFailureError")) {
                                                                            Toast.makeText(ProductDetailsActivity.this, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                                        } else {
                                                                            Toast.makeText(ProductDetailsActivity.this, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                }) {
                                                                    //* Passing some request headers*
                                                                    @Override
                                                                    public Map getHeaders() throws AuthFailureError {
                                                                        HashMap headers = new HashMap();
                                                                        headers.put("Content-Type", "application/json");
                                                                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));

                                                                        Log.e("getHeaders: ", headers.toString());

                                                                        return headers;
                                                                    }
                                                                };

                                                                MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);

                                                            }
                                                        });

                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                    MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                    Log.e("error", e.getMessage());
                                                }


                                            }
                                        }, new Response.ErrorListener() {

                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Log.e("ERROR", error.toString());
                                                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                                if (error.toString().contains("ConnectException")) {
                                                    MethodClass.network_error_alert(ProductDetailsActivity.this);
                                                } else {
                                                    MethodClass.error_alert(ProductDetailsActivity.this);
                                                }

                                            }
                                        }){
                                            //* Passing some request headers*
                                            @Override
                                            public Map getHeaders() throws AuthFailureError {
                                                HashMap headers = new HashMap();
                                                headers.put("Content-Type", "application/json");
                                                if(!PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", "").equals("")){
                                                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));
                                                }
                                                Log.e("getHeaders: ", headers.toString());

                                                return headers;
                                            }
                                        };
                                        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                        // Access the RequestQueue through your singleton class.
                                        MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsObjRequest);
                                    } else {
                                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                        return;
                                    }
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(ProductDetailsActivity.this);
                    } else {
                        MethodClass.error_alert(ProductDetailsActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));
                    }
                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

    }



    public class StringWithTag {
        public String string;
        public String string2;
        public String string3;
        public Object tag;

        public StringWithTag(String stringPart,String stringPart2,String stringPart3, Object tagPart) {
            string = stringPart;
            string2 = stringPart2;
            string3 = stringPart3;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }


    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            String product_pic_url = PRODUCT_IMG_URL+mStrings[position];

            Log.e("PRO_URL", product_pic_url );
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setBackgroundColor(getResources().getColor(R.color.white));
            Picasso.get()
                    .load(product_pic_url)
                    .placeholder(R.drawable.home_10) //this is optional the image to display while the url image is downloading
                    .error(R.drawable.home_10)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(imageView);
        }
    };
//    public void getList() {
//        for (int i = 0; i < 10; i++) {
//            HashMap<String, String> map = new HashMap<String, String>();
//            map.put("TEST", "Test");
//            orderlist.add(map);
//        }
//        Home3Adapter adapter = new Home3Adapter(ProductDetailsActivity.this, orderlist);
//        recycler_view2.setAdapter(adapter);
//    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(ProductDetailsActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                        Intent I  = new Intent(ProductDetailsActivity.this,BuyerAddressBookActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ProductDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){
                        Intent I  = new Intent(ProductDetailsActivity.this,EditProfileActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ProductDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(ProductDetailsActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ProductDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(ProductDetailsActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ProductDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(ProductDetailsActivity.this,OrderListActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(ProductDetailsActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(ProductDetailsActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(ProductDetailsActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(ProductDetailsActivity.this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(ProductDetailsActivity.this,SignInActivity.class);
            startActivity(I);
        }
    }
    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv;
        ImageView home_img,browse_img,cart_img,login_img;

        LinearLayout login_layout,cart_layout,browse_layout,home_layout;

        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);

        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);


        login_layout=(LinearLayout)findViewById(R.id.login_layout);
        cart_layout=(LinearLayout)findViewById(R.id.cart_layout);
        browse_layout=(LinearLayout)findViewById(R.id.browse_layout);
        home_layout=(LinearLayout)findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
                MethodClass.placeName(this,gAdd);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }

}
