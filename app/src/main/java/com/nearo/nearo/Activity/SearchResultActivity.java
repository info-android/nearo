package com.nearo.nearo.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.AddressbookAdapter;
import com.nearo.nearo.Adapter.SearchAdapter;
import com.nearo.nearo.Adapter.SellerProductAdapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.COM_IMG_URL;
import static com.nearo.nearo.Helper.Constants.FILTER_MAX;
import static com.nearo.nearo.Helper.Constants.FILTER_MIN;
import static com.nearo.nearo.Helper.Constants.HIGHEST_PRICE;
import static com.nearo.nearo.Helper.Constants.IS_FAV;
import static com.nearo.nearo.Helper.Constants.IS_SORT;
import static com.nearo.nearo.Helper.Constants.KEYWORD;
import static com.nearo.nearo.Helper.Constants.LOWEST_PRICE;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_DISC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class SearchResultActivity extends AppCompatActivity {
    private RecyclerView recycler_view4;
    private ArrayList<HashMap<String, String>> orderlist;
    private Boolean is_logged_in = false;
    private TextView placeName;
    private TextView totProduct;
    private String user_id = "";
    private String cat_id = "";
    private int tot = 0;
    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private LinearLayout filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        recycler_view4  = findViewById(R.id.recycler_view4);
        placeName = findViewById(R.id.placeName);
        totProduct = findViewById(R.id.totProduct);
        filter = findViewById(R.id.filter);
        user_id = PreferenceManager.getDefaultSharedPreferences(SearchResultActivity.this).getString("user_id","");
        bottumLaySetColor("browse");
        MethodClass.placeName(SearchResultActivity.this,placeName);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(SearchResultActivity.this).getBoolean("is_logged_in",false);

        if(getIntent().getExtras() != null){
            cat_id = getIntent().getStringExtra("cat_id");
        }
        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(SearchResultActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(SearchResultActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(SearchResultActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(SearchResultActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        getList();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                placeName.setText(place.getAddress());
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

        if (requestCode == 3) {
            if(resultCode == RESULT_OK){
                getList();
            }
        }


    }

    public void getList() {
        tot = 0;
        if (MethodClass.isNetworkConnected(SearchResultActivity.this)) {
            MethodClass.showProgressDialog(SearchResultActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "search";
            HashMap<String, String> params = new HashMap<String, String>();
            if(is_logged_in){
                params.put("user_id", user_id);
            }else {
                params.put("user_id", "");
            }
            params.put("lat", String.valueOf(USER_LAT));
            params.put("lng", String.valueOf(USER_LANG));
            params.put("cat_id", cat_id);
            params.put("keyword", KEYWORD);
            if(!FILTER_MAX.equals("") && !FILTER_MIN.equals("")){
                params.put("range", FILTER_MIN+"-"+FILTER_MAX);
            }

            if(!IS_SORT.equals("")){
                params.put("sort_by", IS_SORT);
            }


            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(SearchResultActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(SearchResultActivity.this, response);
                        if (result_Object != null) {
                            orderlist=new ArrayList<HashMap<String, String>>();
                            KEYWORD = "";
                            String message  = result_Object.getString("message");
                            final String highest_price  = result_Object.getString("highest_price");
                            final String lowest_price  = result_Object.getString("lowest_price");

                            filter.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent I = new Intent(SearchResultActivity.this,FilterActivity.class);
                                    I.putExtra(HIGHEST_PRICE,highest_price);
                                    I.putExtra(LOWEST_PRICE,lowest_price);
                                    startActivityForResult(I,3);
                                }
                            });

                            JSONArray products = result_Object.getJSONArray("products");
                            if (products.length() != 0) {

                                tot = products.length();
                                Log.e("featured_jsonArray", String.valueOf(products.length()));
                                for (int i = 0; i < products.length(); i++) {


                                    HashMap<String, String> map = new HashMap<String, String>();

                                    JSONObject productsObj = products.getJSONObject(i);
                                    String id = productsObj.getString("id");
                                    String names = productsObj.getString("name");
                                    String descriptions = productsObj.getString("description");
                                    String starting_price =productsObj.getString("starting_price");
                                    String starting_disc_price =productsObj.getString("starting_disc_price");

                                    String is_fav = productsObj.getString("is_fav");
                                    String img = productsObj.getString("img");

                                    JSONObject imgObj = new JSONObject(img);

                                    map.put(PRO_ID,id);
                                    map.put(PRO_NAME,names);
                                    map.put(PRO_DESC,descriptions);
                                    map.put(PRO_PRICE,starting_price);
                                    map.put(IS_FAV,is_fav);

                                    map.put(PRO_DISC,starting_disc_price);
                                    map.put(PRO_IMG,imgObj.getString("image"));
                                    orderlist.add(map);
                                }
                            }
                            JSONArray longproducts = result_Object.getJSONArray("longDistanceProducts");
                            if (longproducts.length() != 0) {
                                tot = tot+longproducts.length();
                                Log.e("featured_jsonArray", String.valueOf(longproducts.length()));
                                for (int i = 0; i < longproducts.length(); i++) {

                                    HashMap<String, String> map2 = new HashMap<String, String>();

                                    JSONObject longproductsObj = longproducts.getJSONObject(i);
                                    String id = longproductsObj.getString("id");
                                    String names = longproductsObj.getString("name");
                                    String descriptions = longproductsObj.getString("description");
                                    String starting_price =longproductsObj.getString("starting_price");
                                    String starting_disc_price =longproductsObj.getString("starting_disc_price");

                                    String is_fav = longproductsObj.getString("is_fav");
                                    String img = longproductsObj.getString("img");

                                    JSONObject imgObj = new JSONObject(img);

                                    map2.put(PRO_ID,id);
                                    map2.put(PRO_NAME,names);
                                    map2.put(PRO_DESC,descriptions);
                                    map2.put(PRO_PRICE,starting_price);
                                    map2.put(IS_FAV,is_fav);

                                    map2.put(PRO_DISC,starting_disc_price);
                                    map2.put(PRO_IMG,imgObj.getString("image"));
                                    orderlist.add(map2);

                                }
                            }
                            SearchAdapter adapter = new SearchAdapter(SearchResultActivity.this, orderlist);
                            recycler_view4.setAdapter(adapter);

                            totProduct.setText(String.valueOf(tot)+" product(s) found");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(SearchResultActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(SearchResultActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(SearchResultActivity.this);
                    } else {
                        MethodClass.error_alert(SearchResultActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(SearchResultActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SearchResultActivity.this).getString("token", ""));
                    }
                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }
    public void browse(View view){
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(SearchResultActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                        Intent I  = new Intent(SearchResultActivity.this,BuyerAddressBookActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SearchResultActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){
                        Intent I  = new Intent(SearchResultActivity.this,EditProfileActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SearchResultActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(SearchResultActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SearchResultActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(SearchResultActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SearchResultActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(SearchResultActivity.this,OrderListActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(SearchResultActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(SearchResultActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(SearchResultActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(SearchResultActivity.this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(SearchResultActivity.this,SignInActivity.class);
            startActivity(I);
        }
    }
    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv;
        ImageView home_img,browse_img,cart_img,login_img;

        LinearLayout login_layout,cart_layout,browse_layout,home_layout;

        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);

        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);


        login_layout=(LinearLayout)findViewById(R.id.login_layout);
        cart_layout=(LinearLayout)findViewById(R.id.cart_layout);
        browse_layout=(LinearLayout)findViewById(R.id.browse_layout);
        home_layout=(LinearLayout)findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }
}
