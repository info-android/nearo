package com.nearo.nearo.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Adapter.AddressbookAdapter;
import com.nearo.nearo.Adapter.FavouriteAdapter;
import com.nearo.nearo.Adapter.Home1Adapter;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.ADD_CITY;
import static com.nearo.nearo.Helper.Constants.ADD_COUNTRY;
import static com.nearo.nearo.Helper.Constants.ADD_FAV;
import static com.nearo.nearo.Helper.Constants.ADD_ID;
import static com.nearo.nearo.Helper.Constants.ADD_LOC;
import static com.nearo.nearo.Helper.Constants.ADD_PIN;
import static com.nearo.nearo.Helper.Constants.ADD_STATE;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class BuyerAddressBookActivity extends AppCompatActivity {
    private RecyclerView recycler_view4;
    private ArrayList<HashMap<String, String>> orderlist;
    private Boolean is_logged_in = false;
    private TextView placeName;
    private String user_id = "";
    private EditText searchItem;
    int PLACE_AUTOCOMPLETE_REQUEST_CODES = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_address_book);
        placeName = findViewById(R.id.placeName);
        searchItem  = findViewById(R.id.searchItem);

        user_id = PreferenceManager.getDefaultSharedPreferences(BuyerAddressBookActivity.this).getString("user_id","");
        bottumLaySetColor("more");
        MethodClass.placeName(BuyerAddressBookActivity.this,placeName);
        MethodClass.searchKey(BuyerAddressBookActivity.this,searchItem);

        is_logged_in = PreferenceManager.getDefaultSharedPreferences(BuyerAddressBookActivity.this).getBoolean("is_logged_in",false);
        recycler_view4  = findViewById(R.id.recycler_view4);
        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(BuyerAddressBookActivity.this);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.choose_loc);
                ImageView close = (ImageView) dialog.findViewById(R.id.close);
                final TextView gAdds = (TextView) dialog.findViewById(R.id.gAdd);
                // Initialize Places.
                Places.initialize(BuyerAddressBookActivity.this, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                // Create a new Places client instance.
                PlacesClient placesClient = Places.createClient(BuyerAddressBookActivity.this);
                // Set the fields to specify which types of place data to return.
                final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                gAdds.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.OVERLAY, fields)
                                .build(BuyerAddressBookActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODES);
                        dialog.dismiss();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        getList();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODES) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                USER_LAT = place.getLatLng().latitude;
                USER_LANG = place.getLatLng().longitude;
                MethodClass.placeName(this,placeName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i("PLACESS", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }


    }
    public void getList() {
        if (MethodClass.isNetworkConnected(BuyerAddressBookActivity.this)) {
            MethodClass.showProgressDialog(BuyerAddressBookActivity.this);
            String server_url = getString(R.string.SERVER_URL) + "address-book";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("lat", String.valueOf(USER_LAT));
            params.put("lng", String.valueOf(USER_LANG));
            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(BuyerAddressBookActivity.this);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(BuyerAddressBookActivity.this, response);
                        if (result_Object != null) {


                            JSONArray addressbook = result_Object.getJSONArray("addressbook");
                            if (addressbook.length() != 0) {
                                orderlist=new ArrayList<HashMap<String, String>>();
                                Log.e("featured_jsonArray", String.valueOf(addressbook.length()));
                                for (int i = 0; i < addressbook.length(); i++) {

                                    HashMap<String, String> map = new HashMap<String, String>();

                                    JSONObject productsObj =addressbook.getJSONObject(i);
                                    String id = productsObj.getString("id");
                                    String location = productsObj.getString("location");
                                    String state = productsObj.getString("state");
                                    String city = productsObj.getString("city");
                                    String zip_code =productsObj.getString("zip_code");
                                    String is_shipping =productsObj.getString("is_shipping");

                                    String img = productsObj.getString("country_name");

                                    JSONObject imgObj = new JSONObject(img);

                                    map.put(ADD_ID,id);
                                    map.put(ADD_CITY,city);
                                    map.put(ADD_STATE,state);
                                    map.put(ADD_LOC,location);
                                    map.put(ADD_COUNTRY,imgObj.getString("country_name"));
                                    map.put(ADD_PIN,zip_code);
                                    map.put(ADD_FAV,is_shipping);
                                    orderlist.add(map);

                                }
                                AddressbookAdapter adapter = new AddressbookAdapter(BuyerAddressBookActivity.this, orderlist);
                                recycler_view4.setAdapter(adapter);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(BuyerAddressBookActivity.this);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(BuyerAddressBookActivity.this);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(BuyerAddressBookActivity.this);
                    } else {
                        MethodClass.error_alert(BuyerAddressBookActivity.this);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(BuyerAddressBookActivity.this).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(BuyerAddressBookActivity.this).getString("token", ""));
                    }


                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }

    }
    public void browse(View view){
        Intent I  = new Intent(this,SearchResultActivity.class);
        startActivity(I);
    } public void AddAddress(View view){
        Intent I  = new Intent(this,AddAddressActivity.class);
        startActivity(I);
    }
    public void home(View view){
        Intent I  = new Intent(this,MainActivity.class);
        startActivity(I);
    }
    public void shoppingcart(View view){
        Intent I  = new Intent(this,CartActivity.class);
        startActivity(I);
    }
    public void btmMore(View view){
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(BuyerAddressBookActivity.this, view);
        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        //registering popup with OnMenuItemClickListener

        if(is_logged_in){
            popup.getMenu().findItem(R.id.logout).setVisible(true);
            popup.getMenu().findItem(R.id.signIn).setVisible(false);
        }else{
            popup.getMenu().findItem(R.id.logout).setVisible(false);
            popup.getMenu().findItem(R.id.signIn).setVisible(true);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getTitle().equals("Address Book")){
                    if(is_logged_in){
                    }else{
                        Intent I  = new Intent(BuyerAddressBookActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Edit Profile")){
                    if(is_logged_in){
                        Intent I  = new Intent(BuyerAddressBookActivity.this,EditProfileActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(BuyerAddressBookActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Dashboard")){
                    if(is_logged_in){
                        Intent I  = new Intent(BuyerAddressBookActivity.this,BuyerDashboardActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(BuyerAddressBookActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Favourites")){
                    if(is_logged_in){
                        Intent I  = new Intent(BuyerAddressBookActivity.this,FavouritesActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(BuyerAddressBookActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }
                if(item.getTitle().equals("Orders")){
                    if(is_logged_in){
                        Intent I  = new Intent(BuyerAddressBookActivity.this,OrderListActivity.class);
                        startActivity(I);
                    }else{
                        Intent I  = new Intent(BuyerAddressBookActivity.this,SignInActivity.class);
                        startActivity(I);
                    }

                }if(item.getTitle().equals("Logout")){
                    MethodClass.log_out(BuyerAddressBookActivity.this);
                }if(item.getTitle().equals("Sign In")){
                    Intent I  = new Intent(BuyerAddressBookActivity.this,SignInActivity.class);
                    startActivity(I);
                }
                return true;
            }
        });

        popup.show();//showing popup menu
    }

    public  void  profile(View view){
        if(is_logged_in){
            Intent I = new Intent(BuyerAddressBookActivity.this,BuyerDashboardActivity.class);
            startActivity(I);
        }else{
            Intent I = new Intent(BuyerAddressBookActivity.this,SignInActivity.class);
            startActivity(I);
        }
    }
    private void bottumLaySetColor(String s){
        TextView home_tv,browse_tv,cart_tv,login_tv;
        ImageView home_img,browse_img,cart_img,login_img;

        LinearLayout login_layout,cart_layout,browse_layout,home_layout;

        home_tv=(TextView)findViewById(R.id.home_tv);
        browse_tv=(TextView)findViewById(R.id.browse_tv);
        cart_tv=(TextView)findViewById(R.id.cart_tv);
        login_tv=(TextView)findViewById(R.id.login_tv);

        home_img=(ImageView)findViewById(R.id.home_img);
        browse_img=(ImageView)findViewById(R.id.browse_img);
        cart_img=(ImageView)findViewById(R.id.cart_img);
        login_img=(ImageView)findViewById(R.id.login_img);


        login_layout=(LinearLayout)findViewById(R.id.login_layout);
        cart_layout=(LinearLayout)findViewById(R.id.cart_layout);
        browse_layout=(LinearLayout)findViewById(R.id.browse_layout);
        home_layout=(LinearLayout)findViewById(R.id.home_layout);


        DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        home_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        home_layout.setBackgroundColor(getResources().getColor(R.color.white));

        DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        browse_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        browse_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        cart_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        cart_layout.setBackgroundColor(getResources().getColor(R.color.white));


        DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.hint_clr));
        login_tv.setTextColor(getResources().getColor(R.color.hint_clr));
        login_layout.setBackgroundColor(getResources().getColor(R.color.white));

        if (s.equals("home")){
            home_tv.setTextColor(getResources().getColor(R.color.white));
            DrawableCompat.setTint(home_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            home_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("browse")){
            DrawableCompat.setTint(browse_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            browse_tv.setTextColor(getResources().getColor(R.color.white));
            browse_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("cart")){
            DrawableCompat.setTint(cart_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            cart_tv.setTextColor(getResources().getColor(R.color.white));
            cart_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        if (s.equals("more")){
            DrawableCompat.setTint(login_img.getDrawable(), ContextCompat.getColor(this, R.color.white));
            login_tv.setTextColor(getResources().getColor(R.color.white));
            login_layout.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }
}
