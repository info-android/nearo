package com.nearo.nearo.Helper;

import java.util.ArrayList;

public class Constants {

    public static String DEVICE_ID="DEVICE_ID";


    public  static Double PICK_LANG = null;
    public static Double PICK_LAT = null;


    public static Double USER_LAT = null;
    public static Double USER_LANG = null;

    public static final  String CAT_ID="CAT_ID";
    public static final  String CAT_NAME="CAT_NAME";
    public static final  String CAT_IMG="CAT_IMG";


    public static final  String SLIDER_IMG="SLIDER_IMG";
    public static final  String SLIDER_ID="SLIDER_ID";
    public static final  String SLIDER_HEAD1="SLIDER_HEAD1";
    public static final  String SLIDER_HEAD2="SLIDER_HEAD2";
    public static final  String SLIDER_HEAD3="SLIDER_HEAD3";
    public static final  String SLIDER_URL="SLIDER_URL";

    public static final  String PRO_ID="PRO_ID";
    public static final  String PRO_NAME="PRO_NAME";
    public static final  String PRO_DESC="PRO_DESC";
    public static final  String PRO_PRICE="PRO_PRICE";
    public static final  String PRO_DISC="PRO_DISC";
    public static final  String PRO_IMG="PRO_IMG";
    public static final  String PRO_RECIE="PRO_RECIE";
    public static final  String PRO_REV="PRO_REV";

    public static ArrayList<Integer> USER_SELECTED_HEALTH;


    public static final  String ADD_ID="ADD_ID";
    public static final  String ADD_LOC="ADD_LOC";
    public static final  String ADD_STATE="ADD_STATE";
    public static final  String ADD_CITY="ADD_CITY";
    public static final  String ADD_COUNTRY="ADD_COUNTRY";
    public static final  String ADD_PIN="ADD_PIN";
    public static final  String ADD_FAV="ADD_FAV";

    public static final  String CART_ID="ADD_ID";
    public static final  String CART_M_ID="ADD_LOC";
    public static final  String CART_TOTAL="ADD_STATE";
    public static final  String CART_SUBTOTAL="ADD_CITY";
    public static final  String CART_QUANTITY="ADD_COUNTRY";
    public static final  String CART_SELLER="ADD_PIN";
    public static final  String CART_UNIT="CART_UNIT";
    public static final  String CART_NAME="CART_NAME";



    public static final  String ORD_ID="ORD_ID";
    public static final  String ORD_NO="ORD_NO";
    public static final  String ORD_TOTAL="ORD_TOTAL";
    public static final  String ORD_SUB="ORD_SUB";
    public static final  String ORD_DIS="ORD_DIS";
    public static final  String ORD_DATE="ORD_DATE";
    public static final  String ORD_STAT="ORD_STAT";
    public static final  String ORD_PAY="ORD_PAY";
    public static final  String ORD_QTY="ORD_QTY";




    public static final  String IS_FAV="IS_FAV";
    public static String KEYWORD="";

    public  static String FILTER_MAX = "";
    public  static String FILTER_MIN = "";
    public  static String IS_SORT = "";
    public  static String CURRENCY = "₹";

    public  static  final String HIGHEST_PRICE = "HIGHEST_PRICE";
    public  static  final String LOWEST_PRICE = "LOWEST_PRICE";


    public static final  String COM_ID="COM_ID";
    public static final  String COM_NAME="COM_NAME";
    public static final  String COM_IMG="COM_IMG";
    public static final  String COM_RATE="COM_RATE";
    public static final  String COM_ADD="COM_ADD";


    public static final  String REV_POINT="REV_POINT";
    public static final  String REV_HEAD="REV_HEAD";
    public static final  String REV_COMM="REV_COMM";
    public static final  String REV_DATE="REV_DATE";

    public static String FNAME="";
    public static String LNAME="";
    public static String PROVIDER_ID="";



    public static final  String SUB_NAME="SUB_NAME";


    public static String[] SELLERS;

    public static final  String CAT_IMG_URL="https://www.nearo.in/public/uploads/category/";
    public static final  String BAN_IMG_URL="https://www.nearo.in/public/slider_images/small_slider_image/";
    public static final  String SLIDER_IMG_URL="https://www.nearo.in/public/slider_images/small_slider_image/";
    public static final  String PRODUCT_IMG_URL="https://www.nearo.in/public/uploads/products/thumbs/";
    public static final  String COM_IMG_URL="https://www.nearo.in/public/uploads/profile/picture/";


}
