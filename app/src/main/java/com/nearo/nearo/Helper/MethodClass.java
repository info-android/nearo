package com.nearo.nearo.Helper;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Activity.AddAddressActivity;
import com.nearo.nearo.Activity.EditProfileActivity;
import com.nearo.nearo.Activity.ProductDetailsActivity;
import com.nearo.nearo.Activity.SearchResultActivity;
import com.nearo.nearo.Activity.SellerPublicProfileActivity;
import com.nearo.nearo.Activity.SignInActivity;
import com.nearo.nearo.Activity.SplashActivity;
import com.nearo.nearo.Adapter.SellerProductAdapter;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.COM_IMG_URL;
import static com.nearo.nearo.Helper.Constants.DEVICE_ID;
import static com.nearo.nearo.Helper.Constants.KEYWORD;
import static com.nearo.nearo.Helper.Constants.PICK_LANG;
import static com.nearo.nearo.Helper.Constants.PICK_LAT;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_DISC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class MethodClass {

    static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static Location updateLocation;
    private static  LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                updateLocation = location;
            } else {
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    public static JSONObject Json_rpc_format(HashMap<String, String> params) {

        HashMap<String, Object> main_param = new HashMap<String, Object>();

        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        Log.e("req", new JSONObject(main_param).toString());
        return new JSONObject(main_param);

    }
    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
    public static JSONObject get_result_from_webservice(Activity activity, JSONObject response) {
        JSONObject result = null;
        if (response.has("error")) {

            try {
                String error = response.getString("error");
                JSONObject jsonObject = new JSONObject(error);

                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(jsonObject.getString("message"))
                        .setContentText(jsonObject.getString("meaning"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (response.has("result")) {
            try {
                result = response.getJSONObject("result");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            error_alert(activity);

        }
        return result;

    }
    public static void error_alert(Activity activity) {
        try {
            if (!activity.isFinishing()) {
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText("Something went wrong. Please try after some time.")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void network_error_alert(final Activity activity) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Network Error")
                .setContentText("Please check your your internet connection.")
                .setConfirmText("Settings")
                .setCancelText("Okay")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }
    static Dialog mDialog;

    public static void showProgressDialog(Activity activity) {
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }

    public static void hideProgressDialog(Activity activity) {
        if (mDialog != null) {
            mDialog.dismiss();
        }

    }



    public static void placeName(Activity activity, TextView txtView){
        LocationManager lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        Criteria crit = new Criteria();
        crit.setAccuracy(Criteria.ACCURACY_COARSE);
        String provider = lm.getBestProvider(crit, true);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
        Location locations = lm.getLastKnownLocation(provider);
        List<String> providerList = lm.getAllProviders();
        Log.e("LOC_LONG", String.valueOf(providerList));
        Log.e("LOC_LONG", String.valueOf(updateLocation));

        if(null!=providerList && providerList.size()>0){
            double longitude = USER_LANG;
            double latitude = USER_LAT;
            Log.e("LOC_LONG", String.valueOf(longitude));
            Log.e("LOC_LAT", String.valueOf(latitude));
            Log.e("CURRENT", String.valueOf(longitude));
            Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if(null!=listAddresses&&listAddresses.size()>0){
                    String _Location = listAddresses.get(0).getAddressLine(0);
                    txtView.setText(_Location);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    public static void placeName2(Activity activity, TextView txtView){
        LocationManager lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        Criteria crit = new Criteria();
        crit.setAccuracy(Criteria.ACCURACY_FINE);
        String provider = lm.getBestProvider(crit, true);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
        Location locations = lm.getLastKnownLocation(provider);
        List<String> providerList = lm.getAllProviders();
        Log.e("LOC_LONG", String.valueOf(providerList));
        Log.e("LOC_LONG", String.valueOf(updateLocation));

        if(null!=providerList && providerList.size()>0){
            double longitude = PICK_LANG;
            double latitude = PICK_LAT;
            Log.e("LOC_LONG", String.valueOf(longitude));
            Log.e("LOC_LAT", String.valueOf(latitude));
            Log.e("CURRENT", String.valueOf(longitude));
            Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if(null!=listAddresses&&listAddresses.size()>0){
                    String _Location = listAddresses.get(0).getAddressLine(0);
                    txtView.setText(_Location);
                    Log.e("LOC_LAT", _Location);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    public static void searchKey(final Activity activity, final EditText srch){
        srch.setText(KEYWORD);
        srch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    KEYWORD  = srch.getText().toString();
                    Intent I = new Intent(activity, SearchResultActivity.class);
                    activity.startActivity(I);
                    return true;
                }
                return false;
            }
        });
    }

    public static void log_out(final Activity activity) {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(activity);
        if (AccessToken.getCurrentAccessToken() != null) {
            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse graphResponse) {
                    LoginManager.getInstance().logOut();
                }
            }).executeAsync();
            PreferenceManager.getDefaultSharedPreferences(activity).edit().putBoolean("is_logged_in", false).commit();
            Intent intent = new Intent(activity, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            activity.finish();
        }else {
            showProgressDialog(activity);

            String server_url = activity.getString(R.string.SERVER_URL) + "logout";

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("resp", response.toString());
                    hideProgressDialog(activity);
                    PreferenceManager.getDefaultSharedPreferences(activity).edit().clear().commit();

                    Intent intent = new Intent(activity, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activity.startActivity(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    hideProgressDialog(activity);

                    Log.e("error", error.toString());
                    if (error.toString().contains("AuthFailureError")) {
                        Toast.makeText(activity, "Authentication Failure.Please login again to use the app", Toast.LENGTH_SHORT).show();
                        PreferenceManager.getDefaultSharedPreferences(activity).edit().clear().commit();

                        Intent intent = new Intent(activity, SplashActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                    } else {
                        Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };

            MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
        }


    }

    public static void addTocart(final Activity activity, String pro_id){
        Boolean is_logged_in  = PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in",false);
        if (MethodClass.isNetworkConnected(activity)) {
            MethodClass.showProgressDialog(activity);
            String server_url = activity.getString(R.string.SERVER_URL) + "product-details";
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("product_id", pro_id);
            if(is_logged_in){

            }else {
                params.put("divice_id", DEVICE_ID);
            }

            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("HomeRes", response.toString());

                    try {
                        MethodClass.hideProgressDialog(activity);

                        JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                        if (result_Object != null) {

                            String message  = result_Object.getString("message");

                            JSONObject productsArr = result_Object.getJSONObject("product");

                            String name = productsArr.getString("name");
                            String description = productsArr.getString("description");
                            String category = productsArr.getString("category");
                            String sub_category = productsArr.getString("sub_category");
                            String seller_name = productsArr.getString("seller_name");
                            JSONObject seller_nameObj = new JSONObject(seller_name);
                            final String com_id = seller_nameObj.getString("id");

                            JSONObject catObj = new JSONObject(category);
                            String catName = catObj.getString("name");

                            JSONObject subObj = new JSONObject(sub_category);
                            String subName = subObj.getString("name");
                            String unit_price = productsArr.getString("unit_price");

                            final Dialog dialog = new Dialog(activity);
                            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.add_to_cart);
                            ImageView close = (ImageView) dialog.findViewById(R.id.close);
                            Spinner price_spinner = (Spinner) dialog.findViewById(R.id.price_spinner);
                            ImageView plus = (ImageView) dialog.findViewById(R.id.plus);
                            ImageView minus = (ImageView) dialog.findViewById(R.id.minus);
                            TextView quan = (TextView) dialog.findViewById(R.id.quan);
                            EditText gAdd = (EditText) dialog.findViewById(R.id.gAdd);
                            RecyclerView recycler_view4 = (RecyclerView) dialog.findViewById(R.id.recycler_view4);
                            Button addToCart = (Button) dialog.findViewById(R.id.addToCart);

                            placeName(activity,gAdd);

                            // Initialize Places.
                            Places.initialize(activity, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                            // Create a new Places client instance.
                            PlacesClient placesClient = Places.createClient(activity);
                            // Set the fields to specify which types of place data to return.
                            final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                            gAdd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Autocomplete.IntentBuilder(
                                            AutocompleteActivityMode.OVERLAY, fields)
                                            .build(activity);
                                    activity.startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                                }
                            });

                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();


                            JSONArray unitArr = new JSONArray(unit_price);
                            List<StringWithTag> spinnerArrays = new ArrayList<StringWithTag>();
                            for (int i = 0; i <unitArr.length() ; i++) {
                                String id = unitArr.getJSONObject(i).getString("id");
                                String quantity = unitArr.getJSONObject(i).getString("quantity");
                                String price = unitArr.getJSONObject(i).getString("price");
                                String discount_price = unitArr.getJSONObject(i).getString("discount_price");
                                String unit_name = unitArr.getJSONObject(i).getString("unit_name");



                                JSONObject unitName = new JSONObject(unit_name);
                                String unitN = unitName.getString("name");
                                if(Double.parseDouble(price) == Double.parseDouble(discount_price)){
                                    spinnerArrays.add(new StringWithTag(quantity+unitN+"  ₹"+price,price,discount_price, id));
                                }else{
                                    spinnerArrays.add(new StringWithTag(quantity+unitN+"  ₹"+discount_price,price,discount_price, id));
                                }
                            }
                            ArrayAdapter adapter3 = new ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item, spinnerArrays) {
                            };
                            price_spinner.setAdapter(adapter3);
                            price_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                //for user salutation select
                                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                    // An item was selected. You can retrieve the selected item using
                                    //StringWithTag s
                                    StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);

                                    String prices = s.string2;
                                    String disc_prices = s.string3;

                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    // Another interface callback
                                    return;
                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        MethodClass.hideProgressDialog(activity);
                        Log.e("error", e.getMessage());
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", error.toString());
                    MethodClass.hideProgressDialog(activity);
                    if (error.toString().contains("ConnectException")) {
                        MethodClass.network_error_alert(activity);
                    } else {
                        MethodClass.error_alert(activity);
                    }

                }
            }){
                //* Passing some request headers*
                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    if(!PreferenceManager.getDefaultSharedPreferences(activity).getString("token", "").equals("")){
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                    }
                    Log.e("getHeaders: ", headers.toString());

                    return headers;
                }
            };
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            // Access the RequestQueue through your singleton class.
            MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
        } else {
            Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
            snackbar.show();
            return;
        }
    }

    public static class StringWithTag {
        public String string;
        public String string2;
        public String string3;
        public Object tag;

        public StringWithTag(String stringPart,String stringPart2,String stringPart3, Object tagPart) {
            string = stringPart;
            string2 = stringPart2;
            string3 = stringPart3;
            tag = tagPart;
        }

        @Override
        public String toString() {
            return string;
        }
    }

}
