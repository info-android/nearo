package com.nearo.nearo.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.nearo.nearo.Activity.ForgotActivity;
import com.nearo.nearo.Activity.MainActivity;
import com.nearo.nearo.Activity.SignInActivity;
import com.nearo.nearo.Activity.VerifyActivity;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nearo.nearo.Helper.Constants.DEVICE_ID;
import static com.nearo.nearo.Helper.Constants.FNAME;
import static com.nearo.nearo.Helper.Constants.LNAME;
import static com.nearo.nearo.Helper.Constants.PROVIDER_ID;

public class LoginFragment extends Fragment {
    private TextView terms;
    private TextView signUp;
    private Button login;
    private EditText email, password;
    private LinearLayout fblogin;
    LoginButton login_button;
    private TextView forgot_pass;
    CallbackManager callbackManager;

    public LoginFragment() {
        // MapFragment empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);
        //here Facebook callback Manager
        callbackManager = CallbackManager.Factory.create();
        terms = (TextView) view.findViewById(R.id.terms);
        signUp =  view.findViewById(R.id.signUp);
        login =  view.findViewById(R.id.login);
        email =  view.findViewById(R.id.email);
        password =  view.findViewById(R.id.password);
        forgot_pass =  view.findViewById(R.id.forgot_pass);
        FacebookSdk.sdkInitialize(getActivity());//here initialize Facebook Sdk
        fblogin = (LinearLayout) view.findViewById(R.id.fblogin);
        login_button = (LoginButton) view.findViewById(R.id.login_button);
        String styledText = "By clicking an option below, I agree to the <font color='blue'>Terms of Use</font> and have read the <font color='blue'>Privacy Policy</font>.";
        terms.setText(Html.fromHtml(styledText), TextView.BufferType.SPANNABLE);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SignInActivity)getActivity()).Regi();
            }
        });
        forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ForgotActivity.class);
                startActivity(intent);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!emailValidator(email.getText().toString().trim())){
                    email.setError("Please enter valid email");
                    email.requestFocus();
                    return;
                }
                if (password.getText().toString().trim().length() == 0){
                    password.setError("Please enter password");
                    password.requestFocus();
                    return;
                }

                MethodClass.showProgressDialog(getActivity());
                String server_url = getString(R.string.SERVER_URL) + "login";


                HashMap<String, String> params = new HashMap<String, String>();

                params.put("email", email.getText().toString().trim());
                params.put("password", password.getText().toString().trim());
                params.put("provider", "");
                params.put("provider_id", "");

                params.put("device_type", "A");
                params.put("device_id", DEVICE_ID);

                JSONObject jsonObject = MethodClass.Json_rpc_format(params);

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MethodClass.hideProgressDialog(getActivity());
                        Log.e("resp", response.toString());
                        try {

                            JSONObject jsonObject = MethodClass.get_result_from_webservice(getActivity(), response);
                            if (jsonObject != null) {
                                JSONObject jsonObject1 = new JSONObject(jsonObject.getString("user_details"));
                                String id = jsonObject1.getString("id");
                                String fname = jsonObject1.getString("fname");
                                String lname = jsonObject1.getString("lname");
                                String emails = jsonObject1.getString("email");
                                String image = jsonObject1.getString("profile_pic");
                                String vcode="";
                                if (jsonObject1.has("vcode")){
                                    vcode = jsonObject1.getString("vcode");
                                }
                                String token="";
                                if (jsonObject.has("token")){
                                    token = jsonObject.getString("token");
                                }
                                String status = jsonObject1.getString("status");
                                if (status.equals("A")) {

                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("token", token).commit();
                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("user_name", fname+" "+lname).commit();
                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("user_id", id).commit();
                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("email", emails).commit();
                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putBoolean("is_logged_in", true).commit();
                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("profile_pic", image).commit();

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                } else if (status.equals("U")) {
                                    Intent intent = new Intent(getActivity(), VerifyActivity.class);
                                    intent.putExtra("email", emails);
                                    intent.putExtra("vcode", vcode);
                                    intent.putExtra("type", "S");
                                    startActivity(intent);
                                } else if (status.equals("I")) {
                                    if (!getActivity().isFinishing()) {
                                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                                .setTitleText("Inactive User")
                                                .setContentText("Your account is inactive.Please contact with admin")
                                                .setConfirmText("Ok")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();

                                    }
                                }
                            }


                        } catch (JSONException e) {
                            MethodClass.error_alert(getActivity());
                            Log.e("ERROR", e.toString() );
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(getActivity());
                        if (error.toString().contains("ConnectException")) {
                            MethodClass.network_error_alert(getActivity());
                        } else {
                            MethodClass.error_alert(getActivity());
                        }
                    }
                });

                MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
            }
        });

        //here facebook login click button
        fblogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Profile profile = Profile.getCurrentProfile().getCurrentProfile();
                if (profile != null) {
                    // user has logged in
                    LoginManager.getInstance().logOut();
                    login_button.performClick();
                } else {
                    // user has not logged in
                    login_button.performClick();
                }
            }
        });

        //here facebook call back when come the response
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                String accessToken = loginResult.getAccessToken().getToken();//here facebook accessToken provided by facebook
                Log.i("accessToken", accessToken);

                //here create GraphRequest for get user information  with passing accessToken (loginResult.getAccessToken())
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        //this is response  method of facebook GraphRequest in this method got response(user details)
                        Log.i("LoginActivity", response.toString());
                        try {
                            MethodClass.showProgressDialog(getActivity());
                            //here get facebook user data like id and name;
                            final String id = object.getString("id");
                            final String firstname = object.getString("first_name");
                            final String lastname = object.getString("last_name");


                            String email = "";
                            if (object.has("email")) {
                                email = object.getString("email");
                            }
                            String imageURL = "";
                            JSONObject data = response.getJSONObject();
                            if (data.has("picture")) {
                                imageURL = data.getJSONObject("picture").getJSONObject("data").getString("url");
                            }
                            String server_url = getString(R.string.SERVER_URL) + "facebook-login";

                            HashMap<String, String> params = new HashMap<String, String>();
                            params.put("provider_id", id);

                            params.put("device_type", "A");
                            params.put("device_id", DEVICE_ID);

                            JSONObject jsonObject = MethodClass.Json_rpc_format(params);

                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    MethodClass.hideProgressDialog(getActivity());
                                    Log.e("resp", response.toString());
                                    try {

                                        JSONObject jsonObject = MethodClass.get_result_from_webservice(getActivity(), response);
                                        if (jsonObject != null) {
                                            MethodClass.hideProgressDialog(getActivity());
                                            String mess = jsonObject.getString("message");
                                            if(mess.equals("Sign up")){
                                                FNAME = firstname;
                                                LNAME = lastname;
                                                PROVIDER_ID = id;
                                                ((SignInActivity)getActivity()).Regi();
                                            }else
                                            {
                                                JSONObject jsonObject1 = new JSONObject(jsonObject.getString("user_details"));
                                                String id = jsonObject1.getString("id");
                                                String fname = jsonObject1.getString("fname");
                                                String lname = jsonObject1.getString("lname");
                                                String emails = jsonObject1.getString("email");
                                                String image = jsonObject1.getString("profile_pic");
                                                String vcode="";
                                                if (jsonObject1.has("vcode")){
                                                    vcode = jsonObject1.getString("vcode");
                                                }
                                                String token="";
                                                if (jsonObject.has("token")){
                                                    token = jsonObject.getString("token");
                                                }
                                                String status = jsonObject1.getString("status");
                                                if (status.equals("A")) {

                                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("token", token).commit();
                                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("user_name", fname+" "+lname).commit();
                                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("user_id", id).commit();
                                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("email", emails).commit();
                                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putBoolean("is_logged_in", true).commit();
                                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("profile_pic", image).commit();

                                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                } else if (status.equals("U")) {
                                                    Intent intent = new Intent(getActivity(), VerifyActivity.class);
                                                    intent.putExtra("email", emails);
                                                    intent.putExtra("vcode", vcode);
                                                    intent.putExtra("type", "S");
                                                    startActivity(intent);
                                                } else if (status.equals("I")) {
                                                    if (!getActivity().isFinishing()) {
                                                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                                                .setTitleText("Inactive User")
                                                                .setContentText("Your account is inactive.Please contact with admin")
                                                                .setConfirmText("Ok")
                                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                    @Override
                                                                    public void onClick(SweetAlertDialog sDialog) {
                                                                        sDialog.dismissWithAnimation();
                                                                    }
                                                                })
                                                                .show();

                                                    }
                                                }
                                            }

                                        }


                                    } catch (JSONException e) {
                                        MethodClass.error_alert(getActivity());
                                        Log.e("ERROR", e.toString() );
                                        e.printStackTrace();
                                    }


                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    MethodClass.hideProgressDialog(getActivity());
                                    if (error.toString().contains("ConnectException")) {
                                        MethodClass.network_error_alert(getActivity());
                                    } else {
                                        MethodClass.error_alert(getActivity());
                                    }
                                }
                            });

                            MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
                        } catch (JSONException e) {
                            //here facebook graph request JSONException
                            e.printStackTrace();
                            //Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                //here request user data parameter by Bundle
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender, birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();//here send request to facebook server
            }

            //here facebook onCancel method
            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            //here facebook onError method
            @Override
            public void onError(FacebookException exception) {
                //here network problem any type then come this catch function here and show pop up to something wrong
                System.out.println("onError");
                Toast.makeText(getActivity(), exception.getMessage().toString(), Toast.LENGTH_LONG).show();
                Log.v("LoginActivity", exception.getMessage().toString());
            }
        });
        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}