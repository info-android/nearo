package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.nearo.nearo.Activity.ProductDetailsActivity;
import com.nearo.nearo.Activity.SearchResultActivity;
import com.nearo.nearo.Activity.SignInActivity;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.DEVICE_ID;
import static com.nearo.nearo.Helper.Constants.IS_FAV;
import static com.nearo.nearo.Helper.Constants.PICK_LANG;
import static com.nearo.nearo.Helper.Constants.PICK_LAT;
import static com.nearo.nearo.Helper.Constants.PRODUCT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_DISC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.SELLERS;
import static com.nearo.nearo.Helper.Constants.SUB_NAME;
import static com.nearo.nearo.Helper.Constants.USER_LANG;
import static com.nearo.nearo.Helper.Constants.USER_LAT;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    int quann = 1;
    String price_id = "";
    private ArrayList<HashMap<String, String>> orderlist;
    Boolean is_logged_in = false;
    //innitialize listview adapter
    public SearchAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.search_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");

        if(null != (map.get(IS_FAV))) {
            if (map.get(IS_FAV).equals("Y")) {
                holder.fav.setImageDrawable(activity.getDrawable(R.drawable.home_9));
            } else {
                holder.fav.setImageDrawable(activity.getDrawable(R.drawable.ic_heart));
            }
        }
        holder.fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(is_logged_in){
                    if(map.get(IS_FAV).equals("Y")){
                        MethodClass.showProgressDialog(activity);
                        String server_url = activity.getString(R.string.SERVER_URL) + "add-to-favorite";
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("product_id", map.get(PRO_ID));
                        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("resp", response.toString());
                                try{
                                    MethodClass.hideProgressDialog(activity);

                                    JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                    if (result_Object != null) {
                                        String message = result_Object.getString("message");
                                        holder.fav.setImageDrawable(activity.getDrawable(R.drawable.ic_heart));
                                        ((SearchResultActivity)activity).getList();
                                        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Removed!")
                                                .setContentText("Product removed from your favourite list")
                                                .setConfirmText("Ok")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    MethodClass.hideProgressDialog(activity);
                                    Log.e("error", e.getMessage());
                                }


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                MethodClass.hideProgressDialog(activity);

                                Log.e("error", error.toString());
                                if (error.toString().contains("AuthFailureError")) {
                                    Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }) {
                            //* Passing some request headers*
                            @Override
                            public Map getHeaders() throws AuthFailureError {
                                HashMap headers = new HashMap();
                                headers.put("Content-Type", "application/json");
                                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                Log.e("getHeaders: ", headers.toString());

                                return headers;
                            }
                        };

                        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                    }else{
                        MethodClass.showProgressDialog(activity);
                        String server_url = activity.getString(R.string.SERVER_URL) + "add-to-favorite";
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("product_id", map.get(PRO_ID));
                        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("resp", response.toString());
                                try{
                                    MethodClass.hideProgressDialog(activity);

                                    JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                    if (result_Object != null) {
                                        String message = result_Object.getString("message");
                                        holder.fav.setImageDrawable(activity.getDrawable(R.drawable.home_9));
                                        ((SearchResultActivity)activity).getList();
                                        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                .setTitleText("Added!")
                                                .setContentText("Product added to your favourite list")
                                                .setConfirmText("Ok")
                                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                    @Override
                                                    public void onClick(SweetAlertDialog sDialog) {
                                                        sDialog.dismissWithAnimation();
                                                    }
                                                })
                                                .show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    MethodClass.hideProgressDialog(activity);
                                    Log.e("error", e.getMessage());
                                }


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                MethodClass.hideProgressDialog(activity);

                                Log.e("error", error.toString());
                                if (error.toString().contains("AuthFailureError")) {
                                    Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }) {
                            //* Passing some request headers*
                            @Override
                            public Map getHeaders() throws AuthFailureError {
                                HashMap headers = new HashMap();
                                headers.put("Content-Type", "application/json");
                                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                Log.e("getHeaders: ", headers.toString());

                                return headers;
                            }
                        };

                        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                    }

                }else {
                    new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Login Required!")
                            .setContentText("Please login to add this item as favourite")
                            .setConfirmText("Login")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    Intent I= new Intent(activity, SignInActivity.class);
                                    activity.startActivity(I);
                                }
                            })
                            .show();
                }
            }
        });
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, ProductDetailsActivity.class);
                I.putExtra("pro_id",map.get(PRO_ID));
                activity.startActivity(I);
            }
        });

        if(null != (map.get(PRO_DISC))){
            if(Double.parseDouble(map.get(PRO_PRICE)) == Double.parseDouble(map.get(PRO_DISC))){
                holder.discoount.setVisibility(View.GONE);
                holder.price.setText("Rs."+map.get(PRO_PRICE));
            }else {
                holder.discoount.setText("Upto "+String.format("%.2f",((Double.parseDouble(map.get(PRO_PRICE))-Double.parseDouble(map.get(PRO_DISC)))/Double.parseDouble(map.get(PRO_PRICE)))*100)+"% off");
                holder.discoount.setVisibility(View.VISIBLE);
                holder.price.setText("Rs."+map.get(PRO_DISC));
            }
        }

        Picasso.get().load(PRODUCT_IMG_URL+map.get(PRO_IMG)).placeholder(R.drawable.home_10).error(R.drawable.home_10).into(holder.prdct_iv);
        holder.prdct_title_tv.setText(map.get(PRO_NAME));
        if(null != (map.get(PRO_DESC))){
            holder.prdct_dtls_tv.setText(Html.fromHtml(map.get(PRO_DESC)));
        }
        holder.addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MethodClass.isNetworkConnected(activity)) {
                    MethodClass.showProgressDialog(activity);
                    String server_url = activity.getString(R.string.SERVER_URL) + "product-details";
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("product_id", map.get(PRO_ID));
                    if(is_logged_in){

                    }else {
                        params.put("divice_id", DEVICE_ID);
                    }

                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                    JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("HomeRes", response.toString());

                            try {
                                MethodClass.hideProgressDialog(activity);

                                JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                if (result_Object != null) {

                                    String message  = result_Object.getString("message");

                                    JSONObject productsArr = result_Object.getJSONObject("product");

                                    String name = productsArr.getString("name");
                                    String description = productsArr.getString("description");
                                    String category = productsArr.getString("category");
                                    String sub_category = productsArr.getString("sub_category");
                                    String seller_name = productsArr.getString("seller_name");
                                    JSONObject seller_nameObj = new JSONObject(seller_name);
                                    final String com_id = seller_nameObj.getString("id");

                                    JSONObject catObj = new JSONObject(category);
                                    String catName = catObj.getString("name");

                                    JSONObject subObj = new JSONObject(sub_category);
                                    String subName = subObj.getString("name");
                                    String unit_price = productsArr.getString("unit_price");

                                    final Dialog dialog = new Dialog(activity);
                                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog.setContentView(R.layout.add_to_cart);
                                    ImageView close = (ImageView) dialog.findViewById(R.id.close);
                                    Spinner price_spinner = (Spinner) dialog.findViewById(R.id.price_spinner);
                                    ImageView plus = (ImageView) dialog.findViewById(R.id.plus);
                                    ImageView minus = (ImageView) dialog.findViewById(R.id.minus);
                                    final TextView quan = (TextView) dialog.findViewById(R.id.quan);
                                    final TextView gAdd = (TextView) dialog.findViewById(R.id.gAdd);
                                    final RecyclerView recycler_view4 = (RecyclerView) dialog.findViewById(R.id.recycler_view4);
                                    TextView addToCartss = (TextView) dialog.findViewById(R.id.addToCartss);
                                    Button check = (Button) dialog.findViewById(R.id.check);
                                    final TextView txt = (TextView) dialog.findViewById(R.id.txt);

                                    quan.setText(String.valueOf(quann));

                                    plus.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            quann = quann+1;
                                            quan.setText(String.valueOf(quann));
                                        }
                                    });
                                    minus.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if(quann>1){
                                                quann = quann-1;
                                                quan.setText(String.valueOf(quann));
                                            }
                                        }
                                    });
                                    MethodClass.placeName(activity,gAdd);
                                    // Initialize Places.
                                    Places.initialize(activity, "AIzaSyC1A0Zjdpb5eWY6MCTp_8ZOVAlDkUB4MTY");
                                    // Create a new Places client instance.
                                    PlacesClient placesClient = Places.createClient(activity);
                                    // Set the fields to specify which types of place data to return.
                                    final List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME,Place.Field.ADDRESS);

                                    gAdd.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Autocomplete.IntentBuilder(
                                                    AutocompleteActivityMode.OVERLAY, fields)
                                                    .build(activity);
                                            activity.startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                                            dialog.dismiss();
                                        }
                                    });

                                    close.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.show();


                                    JSONArray unitArr = new JSONArray(unit_price);
                                    List<MethodClass.StringWithTag> spinnerArrays = new ArrayList<MethodClass.StringWithTag>();
                                    for (int i = 0; i <unitArr.length() ; i++) {
                                        String id = unitArr.getJSONObject(i).getString("id");
                                        String quantity = unitArr.getJSONObject(i).getString("quantity");
                                        String price = unitArr.getJSONObject(i).getString("price");
                                        String discount_price = unitArr.getJSONObject(i).getString("discount_price");
                                        String unit_name = unitArr.getJSONObject(i).getString("unit_name");



                                        JSONObject unitName = new JSONObject(unit_name);
                                        String unitN = unitName.getString("name");
                                        if(Double.parseDouble(price) == Double.parseDouble(discount_price)){
                                            spinnerArrays.add(new MethodClass.StringWithTag(quantity+unitN+"  ₹"+price,price,discount_price, id));
                                        }else{
                                            spinnerArrays.add(new MethodClass.StringWithTag(quantity+unitN+"  ₹"+discount_price,price,discount_price, id));
                                        }
                                    }
                                    ArrayAdapter adapter3 = new ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item, spinnerArrays) {
                                    };
                                    price_spinner.setAdapter(adapter3);
                                    price_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        //for user salutation select
                                        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                                            // An item was selected. You can retrieve the selected item using
                                            //StringWithTag s
                                            MethodClass.StringWithTag s = (MethodClass.StringWithTag) parent.getItemAtPosition(pos);
                                            price_id = String.valueOf(s.tag);

                                        }
                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {
                                            // Another interface callback
                                            return;
                                        }
                                    });

                                    check.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            MethodClass.showProgressDialog(activity);
                                            String server_url = activity.getString(R.string.SERVER_URL) + "check-location";
                                            HashMap<String, String> params = new HashMap<String, String>();
                                            params.put("location", gAdd.getText().toString());
                                            params.put("lat",String.valueOf(USER_LAT));
                                            params.put("lng", String.valueOf(USER_LANG));
                                            params.put("seller_id",com_id);
                                            if(is_logged_in){
                                                params.put("user_id",user_id);
                                            }else {
                                                params.put("divice_id",DEVICE_ID);
                                            }
                                            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    Log.e("resp", response.toString());
                                                    try{
                                                        MethodClass.hideProgressDialog(activity);

                                                        JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                                        if (result_Object != null) {
                                                            String message = result_Object.getString("message");
                                                            txt.setText(message);

                                                            JSONArray proArr = result_Object.getJSONArray("product");
                                                            if(proArr.length()>0){
                                                                orderlist = new ArrayList<HashMap<String, String>>();
                                                                SELLERS = new String[proArr.length()];
                                                                for (int i = 0; i <proArr.length() ; i++) {
                                                                    HashMap<String,String> mapa = new HashMap<String, String>();
                                                                    JSONObject pObj = proArr.getJSONObject(i);
                                                                    String id = pObj.getString("id");
                                                                    SELLERS[i] = id;

                                                                    String name = pObj.getString("name");

                                                                    mapa.put(SUB_NAME,name);
                                                                    orderlist.add(mapa);

                                                                }

                                                                cartProAdapter adapter1 = new cartProAdapter(activity, orderlist);
                                                                recycler_view4.setAdapter(adapter1);
                                                            }
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                        MethodClass.hideProgressDialog(activity);
                                                        Log.e("error", e.getMessage());
                                                    }


                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {

                                                    MethodClass.hideProgressDialog(activity);

                                                    Log.e("error", error.toString());
                                                    if (error.toString().contains("AuthFailureError")) {
                                                        Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }) {
                                                //* Passing some request headers*
                                                @Override
                                                public Map getHeaders() throws AuthFailureError {
                                                    HashMap headers = new HashMap();
                                                    headers.put("Content-Type", "application/json");
                                                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                                    Log.e("getHeaders: ", headers.toString());

                                                    return headers;
                                                }
                                            };

                                            MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

                                        }
                                    });
                                    addToCartss.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            MethodClass.showProgressDialog(activity);
                                            String server_url = activity.getString(R.string.SERVER_URL) + "check-location";
                                            HashMap<String, String> params = new HashMap<String, String>();
                                            params.put("location", gAdd.getText().toString());
                                            params.put("lat",String.valueOf(USER_LAT));
                                            params.put("lng", String.valueOf(USER_LANG));
                                            params.put("seller_id",com_id);
                                            if(is_logged_in){
                                                params.put("user_id",user_id);
                                            }else {
                                                params.put("divice_id",DEVICE_ID);
                                            }
                                            JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    Log.e("resp", response.toString());
                                                    try{
                                                        MethodClass.hideProgressDialog(activity);

                                                        JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                                        if (result_Object != null) {
                                                            String message = result_Object.getString("message");
                                                            txt.setText(message);

                                                            JSONArray proArr = result_Object.getJSONArray("product");
                                                            if(proArr.length()>0){
                                                                orderlist = new ArrayList<HashMap<String, String>>();
                                                                SELLERS = new String[proArr.length()];
                                                                for (int i = 0; i <proArr.length() ; i++) {
                                                                    HashMap<String,String> mapa = new HashMap<String, String>();
                                                                    JSONObject pObj = proArr.getJSONObject(i);
                                                                    String id = pObj.getString("id");
                                                                    SELLERS[i] = id;

                                                                    String name = pObj.getString("name");

                                                                    mapa.put(SUB_NAME,name);
                                                                    orderlist.add(mapa);

                                                                }

                                                                cartProAdapter adapter1 = new cartProAdapter(activity, orderlist);
                                                                recycler_view4.setAdapter(adapter1);
                                                            }

                                                            if(proArr.length()>0){
                                                                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                                                                        .setTitleText("Delete Product")
                                                                        .setContentText("If you add this product then the listed cart items will be deleted. Do you want to add this?")
                                                                        .setConfirmText("Yes")
                                                                        .setCancelText("No")
                                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                            @Override
                                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                                sDialog.dismissWithAnimation();
                                                                                MethodClass.showProgressDialog(activity);
                                                                                String server_url = activity.getString(R.string.SERVER_URL) + "add-to-cart";
                                                                                HashMap<String, String> params = new HashMap<String, String>();
                                                                                params.put("location", gAdd.getText().toString());
                                                                                params.put("lat",String.valueOf(USER_LAT));
                                                                                params.put("lng", String.valueOf(USER_LANG));
                                                                                params.put("seller_id",com_id);
                                                                                params.put("product_id",map.get(PRO_ID));
                                                                                params.put("price_id",price_id);
                                                                                params.put("delete_product_id", TextUtils.join(",",SELLERS));
                                                                                params.put("qty",String.valueOf(quann));
                                                                                if(is_logged_in){
                                                                                    params.put("user_id",user_id);
                                                                                }else {
                                                                                    params.put("divice_id",DEVICE_ID);
                                                                                }
                                                                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                                                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                                                    @Override
                                                                                    public void onResponse(JSONObject response) {
                                                                                        Log.e("resp", response.toString());
                                                                                        try{
                                                                                            MethodClass.hideProgressDialog(activity);

                                                                                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                                                                            if (result_Object != null) {
                                                                                                String message = result_Object.getString("message");
                                                                                                dialog.dismiss();
                                                                                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                                                                        .setTitleText("Added")
                                                                                                        .setContentText("Product added to cart")
                                                                                                        .setConfirmText("Okay")
                                                                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                                            @Override
                                                                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                                                                sDialog.dismissWithAnimation();
                                                                                                            }
                                                                                                        })
                                                                                                        .show();
                                                                                            }
                                                                                        } catch (JSONException e) {
                                                                                            e.printStackTrace();
                                                                                            MethodClass.hideProgressDialog(activity);
                                                                                            Log.e("error", e.getMessage());
                                                                                        }


                                                                                    }
                                                                                }, new Response.ErrorListener() {
                                                                                    @Override
                                                                                    public void onErrorResponse(VolleyError error) {

                                                                                        MethodClass.hideProgressDialog(activity);

                                                                                        Log.e("error", error.toString());
                                                                                        if (error.toString().contains("AuthFailureError")) {
                                                                                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                                                        } else {
                                                                                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                                                        }
                                                                                    }
                                                                                }) {
                                                                                    //* Passing some request headers*
                                                                                    @Override
                                                                                    public Map getHeaders() throws AuthFailureError {
                                                                                        HashMap headers = new HashMap();
                                                                                        headers.put("Content-Type", "application/json");
                                                                                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                                                                        Log.e("getHeaders: ", headers.toString());

                                                                                        return headers;
                                                                                    }
                                                                                };

                                                                                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                                                                            }
                                                                        })
                                                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                            @Override
                                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                                sDialog.dismissWithAnimation();
                                                                                dialog.dismiss();
                                                                            }
                                                                        })
                                                                        .show();
                                                            }else {
                                                                MethodClass.showProgressDialog(activity);
                                                                String server_url = activity.getString(R.string.SERVER_URL) + "add-to-cart";
                                                                HashMap<String, String> params = new HashMap<String, String>();
                                                                params.put("location", gAdd.getText().toString());
                                                                params.put("lat",String.valueOf(USER_LAT));
                                                                params.put("lng", String.valueOf(USER_LANG));
                                                                params.put("seller_id",com_id);
                                                                params.put("product_id",map.get(PRO_ID));
                                                                params.put("price_id",price_id);
                                                                params.put("delete_product_id", "");
                                                                params.put("qty",String.valueOf(quann));
                                                                if(is_logged_in){
                                                                    params.put("user_id",user_id);
                                                                }else {
                                                                    params.put("divice_id",DEVICE_ID);
                                                                }
                                                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                                                    @Override
                                                                    public void onResponse(JSONObject response) {
                                                                        Log.e("resp", response.toString());
                                                                        try{
                                                                            MethodClass.hideProgressDialog(activity);

                                                                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                                                            if (result_Object != null) {
                                                                                String message = result_Object.getString("message");
                                                                                dialog.dismiss();
                                                                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                                                        .setTitleText("Added")
                                                                                        .setContentText("Product added to cart")
                                                                                        .setConfirmText("Okay")
                                                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                                            @Override
                                                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                                                sDialog.dismissWithAnimation();
                                                                                            }
                                                                                        })
                                                                                        .show();
                                                                            }
                                                                        } catch (JSONException e) {
                                                                            e.printStackTrace();
                                                                            MethodClass.hideProgressDialog(activity);
                                                                            Log.e("error", e.getMessage());
                                                                        }


                                                                    }
                                                                }, new Response.ErrorListener() {
                                                                    @Override
                                                                    public void onErrorResponse(VolleyError error) {

                                                                        MethodClass.hideProgressDialog(activity);

                                                                        Log.e("error", error.toString());
                                                                        if (error.toString().contains("AuthFailureError")) {
                                                                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                                        } else {
                                                                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                }) {
                                                                    //* Passing some request headers*
                                                                    @Override
                                                                    public Map getHeaders() throws AuthFailureError {
                                                                        HashMap headers = new HashMap();
                                                                        headers.put("Content-Type", "application/json");
                                                                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                                                        Log.e("getHeaders: ", headers.toString());

                                                                        return headers;
                                                                    }
                                                                };

                                                                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                                                            }
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                        MethodClass.hideProgressDialog(activity);
                                                        Log.e("error", e.getMessage());
                                                    }


                                                }
                                            }, new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {

                                                    MethodClass.hideProgressDialog(activity);

                                                    Log.e("error", error.toString());
                                                    if (error.toString().contains("AuthFailureError")) {
                                                        Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }) {
                                                //* Passing some request headers*
                                                @Override
                                                public Map getHeaders() throws AuthFailureError {
                                                    HashMap headers = new HashMap();
                                                    headers.put("Content-Type", "application/json");
                                                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                                    Log.e("getHeaders: ", headers.toString());

                                                    return headers;
                                                }
                                            };

                                            MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);

                                        }
                                    });

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                MethodClass.hideProgressDialog(activity);
                                Log.e("error", e.getMessage());
                            }


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ERROR", error.toString());
                            MethodClass.hideProgressDialog(activity);
                            if (error.toString().contains("ConnectException")) {
                                MethodClass.network_error_alert(activity);
                            } else {
                                MethodClass.error_alert(activity);
                            }

                        }
                    }){
                        //* Passing some request headers*
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            HashMap headers = new HashMap();
                            headers.put("Content-Type", "application/json");
                            if(!PreferenceManager.getDefaultSharedPreferences(activity).getString("token", "").equals("")){
                                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                            }
                            Log.e("getHeaders: ", headers.toString());

                            return headers;
                        }
                    };
                    jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50 * 1000, 3,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    // Access the RequestQueue through your singleton class.
                    MySingleton.getInstance(activity).addToRequestQueue(jsObjRequest);
                } else {
                    Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
            }
        });
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {
        LinearLayout container;
        LinearLayout addToCart;
        ImageView fav;
        ImageView prdct_iv;

        TextView prdct_title_tv,prdct_dtls_tv,price,discoount;

        public OrderVh(View v) {
            super(v);
            container = v.findViewById(R.id.container);
            addToCart = v.findViewById(R.id.addToCart);
            fav = v.findViewById(R.id.fav);
            prdct_iv = v.findViewById(R.id.prdct_iv);
            prdct_title_tv = v.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = v.findViewById(R.id.prdct_dtls_tv);
            price = v.findViewById(R.id.price);
            discoount = v.findViewById(R.id.discoount);
        }
    }
    Dialog mDialog;

}