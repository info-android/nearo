package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nearo.nearo.Activity.MainActivity;
import com.nearo.nearo.Activity.OrderDetailsActivity;
import com.nearo.nearo.Activity.OrderListActivity;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.ORD_DATE;
import static com.nearo.nearo.Helper.Constants.ORD_DIS;
import static com.nearo.nearo.Helper.Constants.ORD_ID;
import static com.nearo.nearo.Helper.Constants.ORD_NO;
import static com.nearo.nearo.Helper.Constants.ORD_QTY;
import static com.nearo.nearo.Helper.Constants.ORD_STAT;
import static com.nearo.nearo.Helper.Constants.ORD_SUB;
import static com.nearo.nearo.Helper.Constants.ORD_TOTAL;
import static com.nearo.nearo.Helper.Constants.PRO_ID;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public OrderListAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.order_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);

        holder.viewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, OrderDetailsActivity.class);
                I.putExtra("ord_no",map.get(ORD_NO));
                activity.startActivity(I);
            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Cancel Order")
                        .setContentText("Do you want to cancel the order?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                MethodClass.showProgressDialog(activity);
                                String server_url = activity.getString(R.string.SERVER_URL) + "cancel-order";
                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("order_number", map.get(ORD_NO));
                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("resp", response.toString());
                                        try{
                                            MethodClass.hideProgressDialog(activity);

                                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                            if (result_Object != null) {
                                                String message = result_Object.getString("message");
                                                ((OrderListActivity)activity).getList();
                                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Order Canceled!")
                                                        .setContentText("Order successfully cancelled")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                            }
                                                        })
                                                        .show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            MethodClass.hideProgressDialog(activity);
                                            Log.e("error", e.getMessage());
                                        }


                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        MethodClass.hideProgressDialog(activity);

                                        Log.e("error", error.toString());
                                        if (error.toString().contains("AuthFailureError")) {
                                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }) {
                                    //* Passing some request headers*
                                    @Override
                                    public Map getHeaders() throws AuthFailureError {
                                        HashMap headers = new HashMap();
                                        headers.put("Content-Type", "application/json");
                                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                        Log.e("getHeaders: ", headers.toString());

                                        return headers;
                                    }
                                };

                                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();

            }
        });
        if(map.get(ORD_STAT).equals("D")){
            holder.remove.setVisibility(View.GONE);
            holder.remo.setVisibility(View.VISIBLE);
            holder.delivered.setVisibility(View.VISIBLE);
            holder.newOrder.setVisibility(View.GONE);
        }else if(map.get(ORD_STAT).equals("C")){
            holder.remove.setVisibility(View.GONE);
            holder.remo.setVisibility(View.VISIBLE);
            holder.delivered.setVisibility(View.GONE);
            holder.newOrder.setVisibility(View.VISIBLE);
            holder.newOrder.setText("Cancelled");
        }else if(map.get(ORD_STAT).equals("S")){
            holder.remove.setVisibility(View.GONE);
            holder.remo.setVisibility(View.VISIBLE);
            holder.delivered.setVisibility(View.GONE);
            holder.newOrder.setVisibility(View.VISIBLE);
            holder.newOrder.setText("Shipped");
        }else {
            holder.remove.setVisibility(View.VISIBLE);
            holder.remo.setVisibility(View.GONE);
            holder.delivered.setVisibility(View.GONE);
            holder.newOrder.setVisibility(View.VISIBLE);
        }

        holder.ord_num.setText(map.get(ORD_NO));
        holder.ord_date.setText(map.get(ORD_DATE));
        holder.ord_dis.setText("₹"+map.get(ORD_DIS));
        holder.ord_sub.setText("₹"+map.get(ORD_SUB));
        holder.ord_total.setText("₹"+map.get(ORD_TOTAL));
        holder.ord_qty.setText(map.get(ORD_QTY));
        holder.ord_pay.setText("Cash On Delivery");

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {


        ImageView remove,viewOrder;

        TextView newOrder,delivered;
        TextView ord_num;
        TextView ord_sub,ord_total;
        TextView ord_dis,ord_qty;
        TextView ord_pay,ord_date;
        LinearLayout remo;

        public OrderVh(View v) {
            super(v);
            remove = v.findViewById(R.id.remove);
            remo = v.findViewById(R.id.remo);
            viewOrder = v.findViewById(R.id.viewOrder);
            newOrder = v.findViewById(R.id.newOrder);
            delivered = v.findViewById(R.id.delivered);
            ord_num = v.findViewById(R.id.ord_num);
            ord_sub = v.findViewById(R.id.ord_sub);
            ord_total = v.findViewById(R.id.ord_total);
            ord_dis = v.findViewById(R.id.ord_dis);
            ord_qty = v.findViewById(R.id.ord_qty);
            ord_pay = v.findViewById(R.id.ord_pay);
            ord_date = v.findViewById(R.id.ord_date);
        }
    }

}
