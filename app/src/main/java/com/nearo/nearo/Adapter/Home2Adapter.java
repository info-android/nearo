package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.nearo.nearo.Helper.Constants.BAN_IMG_URL;
import static com.nearo.nearo.Helper.Constants.CAT_IMG;
import static com.nearo.nearo.Helper.Constants.CAT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.SLIDER_HEAD1;
import static com.nearo.nearo.Helper.Constants.SLIDER_HEAD2;
import static com.nearo.nearo.Helper.Constants.SLIDER_HEAD3;
import static com.nearo.nearo.Helper.Constants.SLIDER_IMG;
import static com.nearo.nearo.Helper.Constants.SLIDER_IMG_URL;

public class Home2Adapter extends RecyclerView.Adapter<Home2Adapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public Home2Adapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.home_item2, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        Picasso.get().load(SLIDER_IMG_URL+map.get(SLIDER_IMG)).placeholder(R.drawable.untitled_7).error(R.drawable.untitled_7).into(holder.artImg);

        holder.head1.setText(map.get(SLIDER_HEAD1));
        holder.head2.setText(map.get(SLIDER_HEAD2));
        holder.head3.setText(map.get(SLIDER_HEAD3));


    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        ImageView artImg;
        private TextView head1,head2,head3;
        public OrderVh(View v) {
            super(v);

            artImg = v.findViewById(R.id.artImg);
            head1 = v.findViewById(R.id.head1);
            head2 = v.findViewById(R.id.head2);
            head3 = v.findViewById(R.id.head3);
        }
    }
    Dialog mDialog;

}