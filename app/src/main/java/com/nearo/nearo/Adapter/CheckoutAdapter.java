package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.nearo.nearo.Helper.Constants.CART_NAME;
import static com.nearo.nearo.Helper.Constants.CART_QUANTITY;
import static com.nearo.nearo.Helper.Constants.CART_SELLER;
import static com.nearo.nearo.Helper.Constants.CART_SUBTOTAL;
import static com.nearo.nearo.Helper.Constants.CART_TOTAL;
import static com.nearo.nearo.Helper.Constants.CART_UNIT;
import static com.nearo.nearo.Helper.Constants.PRODUCT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;

public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public CheckoutAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.checkout_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        holder.pro_name.setText(map.get(CART_NAME));
        holder.pro_quan.setText(map.get(CART_QUANTITY));
        holder.pro_unit.setText(map.get(CART_UNIT));
        holder.pro_tot.setText(map.get(CART_TOTAL));
        holder.pro_sub.setText(map.get(CART_SUBTOTAL));
        holder.pro_sell.setText(map.get(CART_SELLER));
        Picasso.get().load(PRODUCT_IMG_URL+map.get(PRO_IMG)).placeholder(R.drawable.home_10).error(R.drawable.home_10).into(holder.prdct_iv);
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        private TextView pro_name,pro_quan,pro_unit,pro_tot,pro_sub,pro_sell;
        private ImageView prdct_iv;
        public OrderVh(View v) {
            super(v);
            pro_name = v.findViewById(R.id.pro_name);

            pro_quan = v.findViewById(R.id.pro_quan);

            pro_unit = v.findViewById(R.id.pro_unit);
            pro_tot = v.findViewById(R.id.pro_tot);
            pro_sub = v.findViewById(R.id.pro_sub);
            pro_sell = v.findViewById(R.id.pro_sell);
            prdct_iv = v.findViewById(R.id.prdct_iv);
        }
    }
    Dialog mDialog;

}
