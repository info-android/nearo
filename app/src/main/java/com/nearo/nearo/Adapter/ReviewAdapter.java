package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;
import java.util.HashMap;

import static com.nearo.nearo.Helper.Constants.COM_IMG_URL;
import static com.nearo.nearo.Helper.Constants.FNAME;
import static com.nearo.nearo.Helper.Constants.LNAME;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.REV_COMM;
import static com.nearo.nearo.Helper.Constants.REV_DATE;
import static com.nearo.nearo.Helper.Constants.REV_HEAD;
import static com.nearo.nearo.Helper.Constants.REV_POINT;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public ReviewAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.review_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);

        holder.usrName.setText(map.get(FNAME)+" "+map.get(LNAME));

        holder.revDate.setText(map.get(REV_DATE));
        holder.revRatingbar.setRating(Float.parseFloat(map.get(REV_POINT)));

        holder.rev_com.setText(Html.fromHtml(map.get(REV_COMM)));
        holder.rev_head.setText(Html.fromHtml(map.get(REV_HEAD)));

        Picasso.get().load(COM_IMG_URL+map.get(PRO_IMG)).placeholder(R.drawable.user1).error(R.drawable.user1).into(holder.usrImg);

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        ImageView usrImg;
        TextView usrName,revDate,rev_head,rev_com;
        ScaleRatingBar revRatingbar;

        public OrderVh(View v) {
            super(v);

            usrImg = v.findViewById(R.id.usrImg);
            usrName = v.findViewById(R.id.usrName);
            revDate = v.findViewById(R.id.revDate);
            rev_head = v.findViewById(R.id.rev_head);
            rev_com = v.findViewById(R.id.rev_com);
            revRatingbar = v.findViewById(R.id.revRatingbar);

        }
    }
    Dialog mDialog;

}