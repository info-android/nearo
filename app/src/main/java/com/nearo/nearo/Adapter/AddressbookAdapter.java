package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nearo.nearo.Activity.BuyerAddressBookActivity;
import com.nearo.nearo.Activity.EditAddressActivity;
import com.nearo.nearo.Activity.SignInActivity;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.ADD_CITY;
import static com.nearo.nearo.Helper.Constants.ADD_COUNTRY;
import static com.nearo.nearo.Helper.Constants.ADD_FAV;
import static com.nearo.nearo.Helper.Constants.ADD_ID;
import static com.nearo.nearo.Helper.Constants.ADD_LOC;
import static com.nearo.nearo.Helper.Constants.ADD_PIN;
import static com.nearo.nearo.Helper.Constants.ADD_STATE;
import static com.nearo.nearo.Helper.Constants.PRO_ID;

public class AddressbookAdapter extends RecyclerView.Adapter<AddressbookAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public AddressbookAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.addressbook_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);

        holder.address.setText(map.get(ADD_LOC));
        holder.city.setText(map.get(ADD_CITY));
        holder.state.setText(map.get(ADD_STATE));
        holder.country.setText(map.get(ADD_COUNTRY));
        holder.pin.setText(map.get(ADD_PIN));
        if(map.get(ADD_FAV).equals("Y")){
            holder.fav.setImageDrawable(activity.getResources().getDrawable(R.drawable.home_9));
        }else {
            holder.fav.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart));

            holder.fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Make Default!")
                            .setContentText("Do you want to make this address default?")
                            .setConfirmText("Yes")
                            .setCancelText("No")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    MethodClass.showProgressDialog(activity);
                                    String server_url = activity.getString(R.string.SERVER_URL) + "set-default-address";
                                    HashMap<String, String> params = new HashMap<String, String>();
                                    params.put("address_id", map.get(ADD_ID));
                                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Log.e("resp", response.toString());
                                            try{
                                                MethodClass.hideProgressDialog(activity);
                                                JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                                if (result_Object != null) {
                                                    String message = result_Object.getString("message");
                                                    ((BuyerAddressBookActivity)activity).getList();
                                                    new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                            .setTitleText("Added!")
                                                            .setContentText("Address marked as default for your future shopping")
                                                            .setConfirmText("Ok")
                                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                                @Override
                                                                public void onClick(SweetAlertDialog sDialog) {
                                                                    sDialog.dismissWithAnimation();
                                                                }
                                                            })
                                                            .show();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                MethodClass.hideProgressDialog(activity);
                                                Log.e("error", e.getMessage());
                                            }


                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            MethodClass.hideProgressDialog(activity);

                                            Log.e("error", error.toString());
                                            if (error.toString().contains("AuthFailureError")) {
                                                Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }) {
                                        //* Passing some request headers*
                                        @Override
                                        public Map getHeaders() throws AuthFailureError {
                                            HashMap headers = new HashMap();
                                            headers.put("Content-Type", "application/json");
                                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                            Log.e("getHeaders: ", headers.toString());

                                            return headers;
                                        }
                                    };

                                    MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                                }
                            })
                            .show();
                }
            });

        }

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, EditAddressActivity.class);
                I.putExtra("add_id",map.get(ADD_ID));
                activity.startActivity(I);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(activity, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Delete Addres!")
                        .setContentText("Do you want to delete this address?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                MethodClass.showProgressDialog(activity);
                                String server_url = activity.getString(R.string.SERVER_URL) + "delete-address";
                                HashMap<String, String> params = new HashMap<String, String>();
                                params.put("address_id", map.get(ADD_ID));
                                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e("resp", response.toString());
                                        try{
                                            MethodClass.hideProgressDialog(activity);

                                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                            if (result_Object != null) {
                                                String message = result_Object.getString("message");
                                                ((BuyerAddressBookActivity)activity).getList();
                                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                                        .setTitleText("Deleted!")
                                                        .setContentText("Address deleted succesfully")
                                                        .setConfirmText("Ok")
                                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                            @Override
                                                            public void onClick(SweetAlertDialog sDialog) {
                                                                sDialog.dismissWithAnimation();
                                                            }
                                                        })
                                                        .show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            MethodClass.hideProgressDialog(activity);
                                            Log.e("error", e.getMessage());
                                        }


                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                        MethodClass.hideProgressDialog(activity);

                                        Log.e("error", error.toString());
                                        if (error.toString().contains("AuthFailureError")) {
                                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }) {
                                    //* Passing some request headers*
                                    @Override
                                    public Map getHeaders() throws AuthFailureError {
                                        HashMap headers = new HashMap();
                                        headers.put("Content-Type", "application/json");
                                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                                        Log.e("getHeaders: ", headers.toString());

                                        return headers;
                                    }
                                };

                                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                            }
                        })
                        .show();
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        ImageView fav,edit,delete;

        TextView address,city,state,country,pin;

        public OrderVh(View v) {
            super(v);

            fav = v.findViewById(R.id.fav);
            edit = v.findViewById(R.id.edit);
            delete = v.findViewById(R.id.delete);
            address = v.findViewById(R.id.address);
            city = v.findViewById(R.id.city);
            state = v.findViewById(R.id.state);
            country = v.findViewById(R.id.country);
            pin = v.findViewById(R.id.pin);

        }
    }
    Dialog mDialog;

}