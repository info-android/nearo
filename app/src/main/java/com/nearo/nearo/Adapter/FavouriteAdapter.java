package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nearo.nearo.Activity.FavouritesActivity;
import com.nearo.nearo.Activity.ProductDetailsActivity;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.PRODUCT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_DISC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public FavouriteAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.favourite_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);

        Picasso.get().load(PRODUCT_IMG_URL+map.get(PRO_IMG)).placeholder(R.drawable.home_10).error(R.drawable.home_10).into(holder.prdct_iv);
        holder.prdct_title_tv.setText(map.get(PRO_NAME));
        if(null != (map.get(PRO_DESC))){
            holder.prdct_dtls_tv.setText(Html.fromHtml(map.get(PRO_DESC)));
        }
        holder.price.setText("Rs."+map.get(PRO_PRICE));
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent I = new Intent(activity, ProductDetailsActivity.class);
                I.putExtra("pro_id",map.get(PRO_ID));
                activity.startActivity(I);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.showProgressDialog(activity);
                String server_url = activity.getString(R.string.SERVER_URL) + "add-to-favorite";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("product_id", map.get(PRO_ID));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            MethodClass.hideProgressDialog(activity);

                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String message = result_Object.getString("message");
                                ((FavouritesActivity)activity).getList();
                                new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Removed!")
                                        .setContentText("Product removed from your favourite list")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sDialog) {
                                                sDialog.dismissWithAnimation();
                                            }
                                        })
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(activity);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(activity);

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
            }
        });
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        ImageView prdct_iv;
        ImageView delete;
        CardView container;
        TextView prdct_title_tv,prdct_dtls_tv,price;
        public OrderVh(View v) {
            super(v);
            prdct_iv = v.findViewById(R.id.prdct_iv);
            prdct_title_tv = v.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = v.findViewById(R.id.prdct_dtls_tv);
            price = v.findViewById(R.id.price);
            container = v.findViewById(R.id.container);
            delete = v.findViewById(R.id.delete);
        }
    }
    Dialog mDialog;

}
