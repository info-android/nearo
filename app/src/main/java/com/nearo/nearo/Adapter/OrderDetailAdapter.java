package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nearo.nearo.Activity.ReviewActivity;
import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.nearo.nearo.Helper.Constants.CART_ID;
import static com.nearo.nearo.Helper.Constants.PRODUCT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.PRO_DESC;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;
import static com.nearo.nearo.Helper.Constants.PRO_NAME;
import static com.nearo.nearo.Helper.Constants.PRO_PRICE;
import static com.nearo.nearo.Helper.Constants.PRO_RECIE;
import static com.nearo.nearo.Helper.Constants.PRO_REV;
import static com.nearo.nearo.Helper.Constants.USER_SELECTED_HEALTH;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    Integer selectedPosition = -1;

    //innitialize listview adapter
    public OrderDetailAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.order_det_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);

        if(map.get(PRO_RECIE).equals("N")){
            holder.chk.setVisibility(View.VISIBLE);
        }else {
            holder.chk.setVisibility(View.GONE);
        }

        if(map.get(PRO_REV).equals("0")){
            holder.giveRev.setVisibility(View.VISIBLE);
            holder.giveRev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent I = new Intent(activity, ReviewActivity.class);
                    I.putExtra("ord_id",map.get(PRO_ID));
                    I.putExtra("pro_id",map.get(CART_ID));
                    activity.startActivity(I);
                }
            });
        }else {
            holder.giveRev.setVisibility(View.GONE);
        }

        holder.prdct_title_tv.setText(map.get(PRO_NAME));
        holder.prdct_dtls_tv.setText(Html.fromHtml(map.get(PRO_DESC)));
        holder.pro_price.setText("₹"+map.get(PRO_PRICE));

        Picasso.get().load(PRODUCT_IMG_URL+map.get(PRO_IMG)).placeholder(R.drawable.home_10).error(R.drawable.home_10).into(holder.prdct_iv);


        holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //do your toast programming here.
                if(b){
                    for(int i=0;i<USER_SELECTED_HEALTH.size();i++)
                    {
                        if(USER_SELECTED_HEALTH.get(i) == Integer.valueOf(map.get(PRO_ID)))
                        {
                            USER_SELECTED_HEALTH.remove(i);
                        }

                    }
                    USER_SELECTED_HEALTH.add(Integer.valueOf(map.get(PRO_ID)));
                }else{
                    for(int i=0;i<USER_SELECTED_HEALTH.size();i++)
                    {
                        if(USER_SELECTED_HEALTH.get(i) == Integer.valueOf(map.get(PRO_ID)))
                        {
                            USER_SELECTED_HEALTH.remove(i);
                        }
                    }
                }
            }
        });
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.chk.isChecked()){
                    holder.chk.setChecked(false);
                    for(int i=0;i<USER_SELECTED_HEALTH.size();i++)
                    {
                        if(USER_SELECTED_HEALTH.get(i) == Integer.valueOf(map.get(PRO_ID)))
                        {
                            USER_SELECTED_HEALTH.remove(i);
                        }
                    }

                }else {
                    holder.chk.setChecked(true);
                    for(int i=0;i<USER_SELECTED_HEALTH.size();i++)
                    {
                        if(USER_SELECTED_HEALTH.get(i) == Integer.valueOf(map.get(PRO_ID)))
                        {
                            USER_SELECTED_HEALTH.remove(i);
                        }

                    }
                    USER_SELECTED_HEALTH.add(Integer.valueOf(map.get(PRO_ID)));

                }
            }
        });
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        CheckBox chk;
        TextView prdct_title_tv,prdct_dtls_tv,pro_price,giveRev;
        ImageView prdct_iv;
        LinearLayout container;
        public OrderVh(View v) {
            super(v);
            chk = v.findViewById(R.id.chk);
            prdct_title_tv = v.findViewById(R.id.prdct_title_tv);
            prdct_dtls_tv = v.findViewById(R.id.prdct_dtls_tv);
            pro_price = v.findViewById(R.id.pro_price);
            prdct_iv = v.findViewById(R.id.prdct_iv);
            giveRev = v.findViewById(R.id.giveRev);
            container = v.findViewById(R.id.container);


        }
    }

    Dialog mDialog;
}
