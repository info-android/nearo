package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nearo.nearo.Activity.CartActivity;
import com.nearo.nearo.Activity.MainActivity;
import com.nearo.nearo.Helper.MethodClass;
import com.nearo.nearo.Helper.MySingleton;
import com.nearo.nearo.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nearo.nearo.Helper.Constants.CART_ID;
import static com.nearo.nearo.Helper.Constants.CART_M_ID;
import static com.nearo.nearo.Helper.Constants.CART_NAME;
import static com.nearo.nearo.Helper.Constants.CART_QUANTITY;
import static com.nearo.nearo.Helper.Constants.CART_SELLER;
import static com.nearo.nearo.Helper.Constants.CART_SUBTOTAL;
import static com.nearo.nearo.Helper.Constants.CART_TOTAL;
import static com.nearo.nearo.Helper.Constants.CART_UNIT;
import static com.nearo.nearo.Helper.Constants.DEVICE_ID;
import static com.nearo.nearo.Helper.Constants.PRODUCT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.PRO_ID;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    Boolean is_logged_in = false;
    int quann;
    //innitialize listview adapter
    public CartAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cart_layout, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        is_logged_in = PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in",false);
        user_id = PreferenceManager.getDefaultSharedPreferences(activity).getString("user_id","");

        quann = Integer.parseInt(map.get(CART_QUANTITY));
        holder.pro_name.setText(map.get(CART_NAME));
        holder.pro_quan.setText(map.get(CART_QUANTITY));
        holder.pro_unit.setText(map.get(CART_UNIT));
        holder.pro_tot.setText(map.get(CART_TOTAL));
        holder.pro_sub.setText(map.get(CART_SUBTOTAL));
        holder.pro_sell.setText(map.get(CART_SELLER));

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.showProgressDialog(activity);
                String server_url = activity.getString(R.string.SERVER_URL) + "update-cart-qty";
                HashMap<String, String> params = new HashMap<String, String>();
                if(is_logged_in){
                    params.put("user_id",user_id);
                }else {
                    params.put("divice_id",DEVICE_ID);
                }
                params.put("qty", String.valueOf(quann+1));
                params.put("cart_details_id", map.get(CART_ID));
                params.put("cart_master_id", map.get(CART_M_ID));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            MethodClass.hideProgressDialog(activity);

                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String message = result_Object.getString("message");
                                ((CartActivity)activity).getList();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(activity);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(activity);

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);


            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(quann>1){
                    MethodClass.showProgressDialog(activity);
                    String server_url = activity.getString(R.string.SERVER_URL) + "update-cart-qty";
                    HashMap<String, String> params = new HashMap<String, String>();
                    if(is_logged_in){
                        params.put("user_id",user_id);
                    }else {
                        params.put("divice_id",DEVICE_ID);
                    }
                    params.put("qty", String.valueOf(quann-1));
                    params.put("cart_details_id", map.get(CART_ID));
                    params.put("cart_master_id", map.get(CART_M_ID));
                    JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("resp", response.toString());
                            try{
                                MethodClass.hideProgressDialog(activity);

                                JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                                if (result_Object != null) {
                                    String message = result_Object.getString("message");
                                    ((CartActivity)activity).getList();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                MethodClass.hideProgressDialog(activity);
                                Log.e("error", e.getMessage());
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            MethodClass.hideProgressDialog(activity);

                            Log.e("error", error.toString());
                            if (error.toString().contains("AuthFailureError")) {
                                Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }) {
                        //* Passing some request headers*
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            HashMap headers = new HashMap();
                            headers.put("Content-Type", "application/json");
                            headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                            Log.e("getHeaders: ", headers.toString());

                            return headers;
                        }
                    };

                    MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                }



            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MethodClass.showProgressDialog(activity);
                String server_url = activity.getString(R.string.SERVER_URL) + "remove-cart";
                HashMap<String, String> params = new HashMap<String, String>();
                if(is_logged_in){
                    params.put("user_id",user_id);
                }else {
                    params.put("divice_id",DEVICE_ID);
                }
                params.put("cart_details_id", map.get(CART_ID));
                params.put("cart_master_id", map.get(CART_M_ID));
                JSONObject jsonObject = MethodClass.Json_rpc_format(params);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("resp", response.toString());
                        try{
                            MethodClass.hideProgressDialog(activity);

                            JSONObject result_Object = MethodClass.get_result_from_webservice(activity, response);
                            if (result_Object != null) {
                                String message = result_Object.getString("message");
                                ((CartActivity)activity).getList();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MethodClass.hideProgressDialog(activity);
                            Log.e("error", e.getMessage());
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        MethodClass.hideProgressDialog(activity);

                        Log.e("error", error.toString());
                        if (error.toString().contains("AuthFailureError")) {
                            Toast.makeText(activity, "Authentication Failure.Please uninstall the app and reinstall it.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity, "Something went wrong.....", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                    //* Passing some request headers*
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        HashMap headers = new HashMap();
                        headers.put("Content-Type", "application/json");
                        headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));

                        Log.e("getHeaders: ", headers.toString());

                        return headers;
                    }
                };

                MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);


            }
        });
        Picasso.get().load(PRODUCT_IMG_URL+map.get(PRO_IMG)).placeholder(R.drawable.home_10).error(R.drawable.home_10).into(holder.prdct_iv);
    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        private TextView pro_name,plus,pro_quan,minus,pro_unit,pro_tot,pro_sub,pro_sell;

        ImageView remove;
        ImageView prdct_iv;

        public OrderVh(View v) {
            super(v);

            remove = v.findViewById(R.id.remove);
            pro_name = v.findViewById(R.id.pro_name);
            plus = v.findViewById(R.id.plus);
            pro_quan = v.findViewById(R.id.pro_quan);
            minus = v.findViewById(R.id.minus);
            pro_unit = v.findViewById(R.id.pro_unit);
            pro_tot = v.findViewById(R.id.pro_tot);
            pro_sub = v.findViewById(R.id.pro_sub);
            pro_sell = v.findViewById(R.id.pro_sell);
            prdct_iv = v.findViewById(R.id.prdct_iv);
        }
    }
    Dialog mDialog;

}