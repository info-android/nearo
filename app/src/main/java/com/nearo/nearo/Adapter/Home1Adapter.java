package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nearo.nearo.Activity.SearchResultActivity;
import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.nearo.nearo.Helper.Constants.CAT_ID;
import static com.nearo.nearo.Helper.Constants.CAT_IMG;
import static com.nearo.nearo.Helper.Constants.CAT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.CAT_NAME;

public class Home1Adapter  extends RecyclerView.Adapter<Home1Adapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public Home1Adapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.home_round, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);

        holder.cate_name.setText(map.get(CAT_NAME));
        Picasso.get().load(CAT_IMG_URL+map.get(CAT_IMG)).placeholder(R.drawable.home_5).error(R.drawable.home_5).into(holder.artImg);

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, SearchResultActivity.class);
                I.putExtra("cat_id",map.get(CAT_ID));
                activity.startActivity(I);
            }
        });

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        TextView cate_name;
        CircleImageView artImg;
        LinearLayout container;
        public OrderVh(View v) {
            super(v);
            artImg = v.findViewById(R.id.artImg);
            cate_name = v.findViewById(R.id.cate_name);
            container = v.findViewById(R.id.container);
        }
    }
    Dialog mDialog;

}