package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearo.nearo.R;

import java.util.ArrayList;
import java.util.HashMap;

public class DashOrderAdapter extends RecyclerView.Adapter<DashOrderAdapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
            Activity activity;
            String user_id;
    //innitialize listview adapter
    public DashOrderAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
            super();
            this.activity = activity;
            this.list = list;
            }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.dashboard_order_item, parent, false);
            return new OrderVh(view);
            }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
            return list == null ? 0 : list.size();
            }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
    final HashMap<String, String> map = list.get(position);

            }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {


        public OrderVh(View v) {
            super(v);

        }
    }
        Dialog mDialog;
}
