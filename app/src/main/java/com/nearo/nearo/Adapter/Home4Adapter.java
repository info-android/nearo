package com.nearo.nearo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearo.nearo.Activity.SellerPublicProfileActivity;
import com.nearo.nearo.R;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;
import java.util.HashMap;

import static com.nearo.nearo.Helper.Constants.COM_ADD;
import static com.nearo.nearo.Helper.Constants.COM_ID;
import static com.nearo.nearo.Helper.Constants.COM_IMG;
import static com.nearo.nearo.Helper.Constants.COM_IMG_URL;
import static com.nearo.nearo.Helper.Constants.COM_NAME;
import static com.nearo.nearo.Helper.Constants.COM_RATE;
import static com.nearo.nearo.Helper.Constants.PRODUCT_IMG_URL;
import static com.nearo.nearo.Helper.Constants.PRO_IMG;

public class Home4Adapter extends RecyclerView.Adapter<Home4Adapter.OrderVh> {
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    String user_id;
    //innitialize listview adapter
    public Home4Adapter(Activity activity, ArrayList<HashMap<String, String>> list) {
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public OrderVh onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.comapny_item, parent, false);
        return new OrderVh(view);
    }

    //override function for adapter extend to baseadater
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public void onBindViewHolder(final OrderVh holder, final int position) {
        final HashMap<String, String> map = list.get(position);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(activity, SellerPublicProfileActivity.class);
                I.putExtra("com_id",map.get(COM_ID));
                activity.startActivity(I);
            }
        });

        Picasso.get().load(COM_IMG_URL+map.get(COM_IMG)).placeholder(R.drawable.home_13).error(R.drawable.home_13).into(holder.prdct_iv);

        holder.address.setText(map.get(COM_ADD));
        holder.prdct_title_tv.setText(map.get(COM_NAME));
        holder.simpleRatingBar.setRating(Float.parseFloat(map.get(COM_RATE)));

    }


    //create holder according to id of view page
    public static class OrderVh extends RecyclerView.ViewHolder {

        CardView container;

        ImageView prdct_iv;

        TextView prdct_title_tv;
        TextView address;
        ScaleRatingBar simpleRatingBar;



        public OrderVh(View v) {
            super(v);

            container = v.findViewById(R.id.container);
            prdct_iv = v.findViewById(R.id.prdct_iv);
            prdct_title_tv = v.findViewById(R.id.prdct_title_tv);
            address = v.findViewById(R.id.address);
            simpleRatingBar = v.findViewById(R.id.simpleRatingBar);

        }
    }
    Dialog mDialog;

}